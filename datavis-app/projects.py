"""
This is the people module and supports all the REST actions for the
people data
"""

from models import Project, ProjectSchema


def read_all():
    """
    This function responds to a request for /api/projects
    with the complete lists of projects

    :return:        json string of list of projects
    """
    # Create the list of projects from our data
    project = Project.query.order_by(Project.name).all()

    # Serialize the data for the response
    project_schema = ProjectSchema(many=True)
    data = project_schema.dump(project)
    return data
