"""
This is the samples module and supports all the REST actions for the
samples data
"""

import ast
from datetime import datetime, timedelta

from config import db
from marshmallow import ValidationError
from models import Analysis, Sample, Project, Person, SampleSchema
from sqlalchemy import and_


# utility functions
def add_sample_to_session(sample: dict):
    """
    Validates a new sample against the model schema and adds it to the db session

    :param sample:
    :return:
    """
    error_msg, success_msg = None, None

    try:
        schema = SampleSchema()
        new_sample = schema.load(sample, session=db.session)  # validate schema
    except ValidationError as err:
        error_msg = err.messages
    else:
        db.session.add(new_sample)
        db.session.flush()
        success_msg = schema.dump(new_sample)

    return error_msg, success_msg


# API endpoint actions
def read_all():
    """
    This function responds to a request for /api/samples
    with the complete lists of samples

    :return:        json string of list of samples
    """
    # Create the list of samples from our data
    # sample = Sample.query.order_by(Sample.name).all()
    samples = Sample.query.join(Analysis).order_by(Analysis.name).all()

    # Serialize the data for the response
    formatted_data = []
    for s in samples:
        metadata = ast.literal_eval(s.sample_metadata)
        d = {"id": s.id, "sample": s.name, "analysis": s.analysis.name, **metadata}
        formatted_data.append(d)

    return formatted_data


def combine_and_filter_sample_data(
    project_ids,
    analysis_ids,
    date_start,
    date_end,
    selected_analysts,
    selected_departments,
    excl_sample_ids,
    metadata_filters,
):
    """
    This function responds to a request for /api/samples/combine_and_filter
    with the complete list of samples, merged with all relevant
    sample metadata information

    :return:        list of dictionaries containing combined sample data
    """
    # Create the complete list of samples from our data
    samples = db.session.query(Project, Analysis, Person, Sample).filter(
        Analysis.project_id == Project.id,
        Analysis.analyst_id == Person.id,
        Sample.analysis_id == Analysis.id,
    )

    # Example output when run on the default data:
    # [
    #   (<Project 1>, <Analysis 1>, <Sample 1>),
    #   (<Project 1>, <Analysis 1>, <Sample 2>)
    # ]

    # Apply filters received from API query
    if "all" not in project_ids:
        project_ids = [int(p) for p in project_ids]
        samples = samples.filter(Project.id.in_(project_ids))

    if "all" not in analysis_ids:
        analysis_ids = [int(a) for a in analysis_ids]
        samples = samples.filter(Analysis.id.in_(analysis_ids))

    if "all" not in date_start and "all" not in date_end:
        date_start = datetime.strptime(date_start, "%Y-%m-%d").date()
        date_end = datetime.strptime(date_end, "%Y-%m-%d").date() + timedelta(
            days=1
        )  # +1 seems to be rqd for the range filter to use the correct range
        samples = samples.filter(
            and_(Analysis.date >= date_start, Analysis.date <= date_end)
        )

    if "all" not in selected_analysts:
        samples = samples.filter(Analysis.analyst_id.in_(selected_analysts))

    if "all" not in selected_departments:
        samples = samples.filter(Analysis.department.in_(selected_departments))

    if "none" not in excl_sample_ids:
        excl_sample_ids = [int(s) for s in excl_sample_ids]
        samples = samples.filter(~Sample.id.in_(excl_sample_ids))

    if "none" not in metadata_filters:
        filtered_out_ids = []
        for s in samples.all():
            metadata = ast.literal_eval(s.Sample.sample_metadata)
            for k, v in metadata.items():
                for f in metadata_filters:
                    parts = f.split("}}{{")
                    category = parts[0].replace("{{", "")
                    value = parts[2].replace("}}", "")

                    possible_operations = {
                        "greater-than": ">",
                        "less-than": "<",
                        "equal-to": "=",
                        "contains": "contains",
                        "does not contain": "does not contain",
                    }

                    operation = possible_operations[parts[1]]

                    if k == category:
                        try:
                            if (
                                operation != "contains"
                                and operation != "does not contain"
                            ):
                                try:
                                    float(value)
                                except ValueError:
                                    continue

                                query_string = v + operation + value
                                if not eval(query_string):
                                    filtered_out_ids.append(s.Sample.id)
                            elif operation == "contains":
                                if value not in v:
                                    filtered_out_ids.append(s.Sample.id)
                            else:
                                if value in v:
                                    filtered_out_ids.append(s.Sample.id)
                        except Exception as e:
                            print(e)

        samples = samples.filter(~Sample.id.in_(filtered_out_ids))

    samples = samples.all()

    # Serialize the data for the response
    formatted_data = []
    for s in samples:
        metadata = ast.literal_eval(s.Sample.sample_metadata)
        d = {
            "id": s.Sample.id,
            "sample": s.Sample.name,
            "project": s.Project.name,
            "analysis": s.Analysis.name,
            **metadata,
        }
        formatted_data.append(d)

    return formatted_data


def fetch_metadata(sample_ids):
    """
    This function responds to a request for /api/samples/metadata
    with a list of available metadata fields for a list of sample ids

    :return:        list of available metadata fields
    """
    # Fetch all sample information
    samples = db.session.query(Sample)

    # Apply filters received from API query
    if "none" in sample_ids:
        sample_ids = [-1]
        samples = samples.filter(Sample.id.in_(sample_ids))
    elif "all" not in sample_ids:
        sample_ids = [int(s) for s in sample_ids]
        samples = samples.filter(Sample.id.in_(sample_ids))

    samples = samples.all()

    # Serialize the data for the response
    all_metadata = set()  # ensures unique entries
    for s in samples:
        metadata = ast.literal_eval(s.sample_metadata)
        all_metadata.update(list(metadata.keys()))

    all_metadata = list(all_metadata)

    return all_metadata
