"""
This is the samples module and supports all the REST actions for the
samples data
"""

import ast
import json

import altair as alt
import pandas as pd
from config import db
from marshmallow import ValidationError
from models import Result, Sample, Analysis, Project, ResultSchema


# utility functions
def add_result_to_session(result: dict):
    """
    Validates a new result against the model schema and adds it to the db session

    :param result:
    :return:
    """
    error_msg, success_msg = None, None

    try:
        schema = ResultSchema()
        new_result = schema.load(result, session=db.session)  # validate schema
    except ValidationError as err:
        error_msg = err.messages
    else:
        db.session.add(new_result)
        db.session.flush()
        success_msg = schema.dump(new_result)

    return error_msg, success_msg


# API endpoint actions
def read_all():
    """
    This function responds to a request for /api/results
    with the complete list of results

    :return:        json string of list of results
    """
    # Create the list of results from our data
    results = Result.query.join(Sample).order_by(Sample.name).all()

    # Serialize the data for the response
    formatted_data = []
    for r in results:
        metrics_data = ast.literal_eval(r.metrics)
        d = {"result_id": r.id, "sample": r.sample.name, **metrics_data}
        formatted_data.append(d)

    return formatted_data


def read_metrics(sample_ids):
    """
    This function responds to a request for /api/results/metrics
    with the list of available metrics for the Intersection of the set

    :return:        json string of list of results
    """
    # Create the list of results from our data
    results = Result.query.join(Sample).order_by(Sample.name)

    # Apply filters received from API query
    if "none" in sample_ids:
        return []
    elif "all" in sample_ids:
        pass
    else:
        sample_ids = [int(s) for s in sample_ids]
        results = results.filter(Sample.id.in_(sample_ids))

    results = results.all()

    # Serialize the data for the response
    formatted_data = []
    for r in results:
        metrics_data = ast.literal_eval(r.metrics)
        formatted_data.extend(list(metrics_data.keys()))

    formatted_data = list(set(formatted_data))  # remove duplicate entries

    return formatted_data


def plot_data(sample_ids):
    """
    This function responds to a request for /api/results/plot.
    It queries the database for all results belonging to the supplied
    sample Ids and builds and returns an altair plot specification
    based off these results

    :param sample_ids:
    :return: json string describing the altair plot specification
    """
    # Create the complete list of results from our data
    results = db.session.query(Sample, Result).filter(Result.sample_id == Sample.id)

    # Example output when run on the default data:
    # [
    #   (
    #     < Sample(id='1', name='S01', sample_metadata='{'kit': 'RNAHyper',
    #     'enrichment': 'riboerase'}', analysis_id='2' >, < Result(id='1', metrics='{
    #     'mapping_rate': 0.945, 'residual_rRNA_ % ': 1.23}', sample_id='1' >
    #   ),
    #   (...),
    #   (...),
    #   (...)
    # ]

    # Apply filters received from API query
    if "none" in sample_ids:
        return {}
    else:
        sample_ids = [int(s) for s in sample_ids]
        results = results.filter(Sample.id.in_(sample_ids)).all()

    formatted_data = []

    # Serialize the data for the response
    for r in results:
        metadata = ast.literal_eval(r.Sample.sample_metadata)
        metrics = ast.literal_eval(r.Result.metrics)

        d = {
            "sample_id": r.Sample.id,
            "sample_name": r.Sample.name,
            **metadata,
            **metrics,
        }
        formatted_data.append(d)

    # Build an alair plot spec from results data
    df = pd.DataFrame(formatted_data)

    plot_spec = (
        alt.Chart(df, width=500, height=300)
        .mark_point()
        .encode(x="sample_name", y="mapping_rate", color="kit", tooltip=["sample_name"])
        .interactive()
        .properties(title="mapping rate vs sample")
    )

    plot_spec = plot_spec.to_json()  # exports python chart into altair json text spec
    plot_spec = json.loads(plot_spec)  # converts json text string into js object

    return plot_spec


def generate_plot_spec(sample_ids, metric_name, plot_type, grouping_category):
    """
    This function responds to a request for /api/results/plot_data.
    It queries the database for all results belonging to the supplied
    sample Ids and builds and returns an altair plot specification
    based off these results and the accompanying plotting parameters

    :param sample_ids: list of sample ids to use in plot
    :param metric_name: name of the dependent variable to plot
    :param plot_type: type of the plot to generate ['strip-plot', 'bar-plot', 'line-plot']
    :param grouping_category: metadata category by which to differentiate groups of data
    :return: json string describing the altair plot specification
    """
    # Create the complete list of results from our data
    results = (
        db.session.query(Sample, Result, Analysis)
        .filter(Result.sample_id == Sample.id)
        .filter(Sample.analysis_id == Analysis.id)
    )

    # Example output when run on the default data:
    # [
    #   (
    #     < Sample(id='1', name='S01', sample_metadata='{'kit': 'RNAHyper',
    #     'enrichment': 'riboerase'}', analysis_id='2' >, < Result(id='1', metrics='{
    #     'mapping_rate': 0.945, 'residual_rRNA_ % ': 1.23}', sample_id='1' >
    #   ),
    #   (...),
    #   (...),
    #   (...)
    # ]

    # Apply filters received from API query
    if "none" in sample_ids:
        return {}
    elif "all" in sample_ids:
        pass
    else:
        sample_ids = [int(s) for s in sample_ids]
        results = results.filter(Sample.id.in_(sample_ids))

    results = results.all()

    # Serialize the data for the response
    formatted_data = []

    for r in results:
        metadata = ast.literal_eval(r.Sample.sample_metadata)
        metrics = ast.literal_eval(r.Result.metrics)

        d = {
            "sample_id": r.Sample.id,
            "sample_name": r.Sample.name,
            "analysis": r.Analysis.name,
            **metadata,
            **metrics,
        }
        formatted_data.append(d)

    # Build an alair plot spec from results data
    df = pd.DataFrame(formatted_data)

    plot_spec = alt.Chart(df, width=500, height=300)

    # ensure plot_type is one of the valid options
    if plot_type == "strip-plot":
        plot_spec = plot_spec.mark_point()
    elif plot_type == "bar-plot":
        plot_spec = plot_spec.mark_bar()
    elif plot_type == "line-plot":
        plot_spec = plot_spec.mark_line()
    else:
        return {}

    plot_spec = (
        plot_spec.encode(
            x="sample_name",
            y=metric_name,
            color=grouping_category,
            tooltip=["sample_name", "analysis", metric_name, grouping_category],
        )
        .interactive()
        .properties(title=f"{metric_name} vs sample name")
    )
    plot_spec = plot_spec.to_json()  # exports python chart into altair json text spec
    plot_spec = json.loads(plot_spec)  # converts json text string into js object

    return plot_spec


def generate_dashboard_plots():
    """
    This function responds to a request for /api/results/plot_dashboard.
    It queries the database for all results and builds a series of altair
    plot specifications to be displayed on the user dashboard

    :return: list of json strings describing the altair plot specification
    """
    # Create the complete list of results from our data
    results = (
        db.session.query(Analysis, Project)
        .filter(Analysis.project_id == Project.id)
        .all()
    )

    # Serialize the data for the response
    formatted_data = []
    plot_specs = []
    summed_analyses = {}

    for r in results:
        if r.Project.name not in list(summed_analyses.keys()):
            summed_analyses[r.Project.name] = 1
        else:
            summed_analyses[r.Project.name] += 1

    for proj in list(summed_analyses.keys()):
        d = {"project": proj, "total analyses": summed_analyses[proj]}

        formatted_data.append(d)

    # Build an alair plot spec from results data
    df = pd.DataFrame(formatted_data)

    plot_spec = alt.Chart(df, width=300, height=200).mark_bar()
    plot_spec = plot_spec.encode(x="project", y="total analyses").properties(
        title="Project time allocation by analyses"
    )
    plot_spec = plot_spec.to_json()  # exports python chart into altair json text spec
    plot_spec = json.loads(plot_spec)  # converts json text string into js object
    plot_specs.append(plot_spec)

    formatted_data = []

    for r in results:
        d = {"analysis": r.Analysis.name, "date": str(r.Analysis.date)}
        formatted_data.append(d)

    # Build an alair plot spec from results data
    df = pd.DataFrame(formatted_data)

    plot_spec = alt.Chart(df, width=300, height=200).mark_line()
    plot_spec = plot_spec.encode(x="date", y="count()").properties(
        title="Number of analyses performed by date"
    )
    plot_spec = plot_spec.to_json()  # exports python chart into altair json text spec
    plot_spec = json.loads(plot_spec)  # converts json text string into js object
    plot_specs.append(plot_spec)

    return plot_specs
