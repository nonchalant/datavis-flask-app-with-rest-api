"""
This module contains helper functions shared by different modules
in the codebase
"""


def convert_form_data_to_dict(body, valid_keys):
    """
    Format POST/PUT form data for easy consumption by a marshmallow model schema

    Formatting includes the following:
      - sanitize data to include only the keys which are supplied in `valid_keys`
      - add missing keys to body from `valid_keys` and set their values to None
      - replace empty string values with None for valid keys that are present
        (prevents db crashing when inserting "" for certain types such as date fields)

    - Expected body format::

         {
           'undesired_key_1': 'some value',
           'undesired_key_2': 'some other value',
           'name': 'RNAHyper customer complaint',
           'description': 'customer reported high rRNA rate',
           'department': '',
           'analyst_id': '1',
           'project_id': '1'
         }

    - Return format::

        {
          'name': 'RNAHyper customer complaint',
          'description': 'customer reported high rRNA rate',
          'date': None,
          'department': None,
          'analyst_id': '3',
          'project_id': '1'
        }

    :param body: flat dict containing key: value pairs to sanitize
    :param valid_keys: list of valid keys to include in the result
    :return: result dict
    """
    result = {k: body.get(k, "") for k in valid_keys}
    result = {k: (None if v is "" else v) for k, v in result.items()}
    return result
