"""
Contains helper classes and functions for parsing QC Reports
"""
import json
from io import BytesIO
from pathlib import Path

import pandas as pd
from analyses import check_for_analysis, add_analysis_to_session
from common import convert_form_data_to_dict
from config import db
from models import Project
from results import add_result_to_session
from samples import add_sample_to_session


# helper functions
def is_excel(file):
    if ".xlsx" in file:
        return True
    else:
        return False


def filter_on_index(df, desired_keywords=[""]):
    filter_str = "|".join(desired_keywords)
    df = df[df.index.str.contains(filter_str)]
    return df


def read_bytes(file_data):
    return BytesIO(file_data)


def contains_sheets(file, sheet_names=[""]):
    for sheet in sheet_names:
        try:
            pd.read_excel(file, sheet_name=sheet)
        except Exception:
            return False

    return True


# utility functions
def build_samples_from_report(report: object, analysis_id):
    """
    Builds a list of sample dicts from a Report instance and an analysis id

    :param report: instance of Report
    :param analysis_id: the analysis id to which the new samples belong
    :return: list of sample dicts
    """
    samples = []
    for sample_name in report.sample_names:
        metadata = json.dumps(report.sample_metadata)
        sample = {
            "name": sample_name,
            "sample_metadata": metadata,
            "analysis_id": analysis_id,
        }
        samples.append(sample)

    return samples


def build_result(metrics: dict, id):
    """
    Builds a result dict from a metrics dict and a sample id

    :param metrics: dict containing metrics and their values
    :param id: the sample id to which the new results belong
    :return: result dict
    """
    metrics = json.dumps(metrics)
    return {"sample_id": id, "metrics": metrics}


def build_results_from_report(report: object, sample_id_map: dict):
    """
    Builds a list of result dicts from a Report instance and a sample id: name mapping

    :param report: instance of Report
    :param sample_id_map: dict mapping which samples the new results belong to
    :return: list of result dicts
    """
    results = []
    for sample_name in report.sample_names:
        result = build_result(report.results[sample_name], sample_id_map[sample_name])
        results.append(result)

    return results


def make_report_object(file_name, file_contents, project_id):
    """
    Returns appropriate instance of Report from a file

    :return: Report instance
    """
    project_name = Project.query.get(project_id).name
    project_name = QC_PROJECTS.get(project_name, None)

    if project_name:
        report = project_name(f_name=file_name, f_data=file_contents)
        return report
    else:
        return None


# Report classes
class Report:
    """Common base class for all QC Report types"""

    def __init__(self, f_name="", f_data=""):
        self.f_name = f_name
        self.f_data = f_data  # self.text_to_file(file_contents)
        self.sample_names = []
        self.sample_metadata = {}  # {'date': '2019-01-01', 'version': 'v1.0.0'}
        self.results = {}  # {'Correlation to ERCC': 0.988, 'residual_rRNA_%': 1.23}
        self.project_name = ""
        self.field_errors = []
        self.f_type = self.parse_filetype()

    def parse_filetype(self):
        return str(Path(self.f_name).suffix)

    def __repr__(self):
        return f"project_name({self.project_name}) sample_names({self.sample_names}) sample_metadata({self.sample_metadata}) results({self.results})"


class RNASeqV1Report(Report):
    """Parses sample and result information from RNASeq V1 QC reports"""

    def __init__(self, f_name="", f_data=""):
        super().__init__(f_name=f_name, f_data=f_data)
        self.project_name = "RNASeq V1"
        self.sheets = {"data": "QC report", "meta": "Analysis Details"}
        self.metric_fields = [
            "Total Reads Sequenced",
            "Mapping Rate (Human + ERCC) (%)",
            "Correlation to ERCC",
            "End 1% Sense of the ERCC",
            "End 2% Sense of the ERCC",
            "Number of reads mapping to E. coli per 100000",
            "Pass/Fail/NA",
        ]

        self.correct_format: bool = self.validate_format()
        self.sample_names: list[str] = self.parse_sample_names()
        self.results: dict = self.parse_results()
        self.sample_metadata: dict = self.parse_metadata()

    def validate_format(self):
        if not is_excel(self.f_type):
            error = {"File format": "File must be in excel (.xlsx) format."}
            self.field_errors.append(error)
            return False

        sheet_names = list(self.sheets.values())

        if not contains_sheets(read_bytes(self.f_data), sheet_names=sheet_names):
            error = {"File format": f"File must contain worksheets: {sheet_names}."}
            self.field_errors.append(error)
            return False

        return True

    def parse_sample_names(self):
        if not self.correct_format:
            return []

        try:
            df = pd.read_excel(
                read_bytes(self.f_data),
                sheet_name=self.sheets["data"],
                index_col="Description",
            )
            df = filter_on_index(df, desired_keywords=["Reference", "Test"])
            return list(df.index)
        except Exception:
            error = {"File format": f"Incorrect format for {self.project_name} report."}
            self.field_errors.append(error)
            return []

    def parse_results(self):
        if not self.correct_format:
            return {}

        try:
            df = pd.read_excel(
                read_bytes(self.f_data),
                sheet_name=self.sheets["data"],
                index_col="Description",
                na_filter=None,
            )
            df = filter_on_index(df, desired_keywords=["Reference", "Test"])
            df = df[self.metric_fields]
            return df.to_dict(orient="index")
        except Exception:
            error = {"File format": f"Incorrect format for {self.project_name} report."}
            self.field_errors.append(error)
            return {}

    def parse_metadata(self):
        if not self.correct_format:
            return {}

        keywords = ["date", "location", "data identifier", "version number", "analyst"]

        try:
            df = pd.read_excel(
                read_bytes(self.f_data), sheet_name=self.sheets["meta"], index_col=0
            )
            df = filter_on_index(df, desired_keywords=keywords).transpose()
            sample_metadata = df.to_dict(orient="records")[0]
            sample_metadata["date"] = list(df.index)[0]
            return sample_metadata
        except Exception:
            error = {"File format": f"Incorrect format for {self.project_name} report."}
            self.field_errors.append(error)
            return {}


# configuration settings
QC_PROJECTS = {
    "RNASeq v1": RNASeqV1Report,
    # "RNA Hyper": RNAHyperReport,
    # "Dual-index adapters": ClassName,
    # "Dual-index adapters (raw materials)": ClassName,
    # "Single-index adapters": ClassName,
    # "UDI adapters": ClassName,
    # "GlobinErase": ClassName,
    # "RiboErase": ClassName,
}


# API endpoint actions
def add_qc_dataset(body, qc_report):
    """
    Responds to POST request for /api/qc_dataset; adds new qc dataset using
    form data and accompanying QC report file.

    The QC report should be either an .xlsx or .html file in the format produced by the
    respective QC pipeline.

    - Expected body format::

         {
           'name': 'KAPA_MSQ_100',
           'description': 'Standard manufacturing QC run',
           'date': '2018-03-01',
           'analyst_id': '1',
           'project_id': '1',
         }


    :return: json string of analyses data; HTTP status code
    """
    valid_keys = ["name", "description", "date", "analyst_id", "project_id"]
    analysis = convert_form_data_to_dict(body, valid_keys)
    error_response = {"fieldErrors": []}
    success_response = {"data": []}

    # create Report instance from uploaded QC report
    qc_dataset: object = make_report_object(
        qc_report.filename, qc_report.read(), analysis["project_id"]
    )

    # update analysis information with metdata from QC report
    analysis["department"] = "QC"
    analysis["date"] = qc_dataset.sample_metadata["date"]

    # add field errors from QC report
    if qc_dataset.field_errors:
        error_response["fieldErrors"].append(qc_dataset.field_errors)

    # check if the analysis already exists
    if check_for_analysis(analysis) is None:
        # attempt to stage new analysis
        error_msg, success_msg = add_analysis_to_session(analysis)

        if error_msg:
            error_response["fieldErrors"].append(error_msg)
        if success_msg:
            success_response["data"].append(success_msg)

        # attempt to stage new samples
        sample_ids = {}
        analysis_id = success_msg.get("id")

        if analysis_id:
            samples = build_samples_from_report(qc_dataset, analysis_id)

            for sample in samples:
                error_msg, success_msg = add_sample_to_session(sample)

                if error_msg:
                    error_response["fieldErrors"].append(error_msg)
                if success_msg:
                    success_response["data"].append(success_msg)
                    sample_ids[success_msg.get("name")] = success_msg.get("id")

        # attempt to stage new results
        if sample_ids:
            results = build_results_from_report(qc_dataset, sample_ids)
            for result in results:
                error_msg, success_msg = add_result_to_session(result)

                if error_msg:
                    error_response["fieldErrors"].append(error_msg)
                if success_msg:
                    success_response["data"].append(success_msg)
    else:
        error = {"name": ["Field must be unique"]}
        error_response["fieldErrors"].append(error)

    # commit changes and return the appropriate response
    if not error_response["fieldErrors"]:
        db.session.commit()
        return success_response, 200
    else:
        # field_errors = format_error_response(field_errors, table_name='analysis')
        return error_response, 409
