"""
Supports REST actions for analysis data by OpenAPI
"""

from config import db
from marshmallow import ValidationError
from models import Analysis, AnalysisSchema, Project, Person


# helper functions
def check_for_analysis(analysis):
    return Analysis.query.filter(Analysis.name == analysis["name"]).one_or_none()


# utility functions
def add_analysis_to_session(analysis: dict):
    """
    Validates a new analysis against the model schema and adds it to the db session

    :param analysis:
    :return:
    """
    error_msg, success_msg = None, None

    try:
        schema = AnalysisSchema()
        new_analysis = schema.load(analysis, session=db.session)  # validate schema
    except ValidationError as err:
        error_msg = err.messages
    else:
        db.session.add(new_analysis)
        db.session.flush()
        success_msg = schema.dump(new_analysis)

    return error_msg, success_msg


# API endpoint actions
def read_all():
    """
    Responds to a request for /api/analyses; returns all analysis data in human
    readable format.

    - Expected success response::

        HTTP Status Code: 201

        [
          {
            "analyst_id": 3,
            "analyst_name": "Jack",
            "date": "2018-08-01",
            "department": "QC",
            "description": "Routine QC run for RNASeq v1 QC conducted by Eugene",
            "id": 4,
            "name": "KAPA_MSQ_100",
            "project_id": 2,
            "project_name": "RNASeq v1"
          }
        ]

    :return: json string of analyses data; HTTP status code
    """
    # Create the list of analyses from our data
    analysis = Analysis.query.order_by(Analysis.name).all()

    # Serialize the data for the response
    analysis_schema = AnalysisSchema(many=True)
    all_analyses = analysis_schema.dump(analysis)

    # convert ids to human readable names
    for a in all_analyses:
        a["project_name"] = Project.query.get(a["project_id"]).name
        a["analyst_name"] = Person.query.get(a["analyst_id"]).name

    return all_analyses
