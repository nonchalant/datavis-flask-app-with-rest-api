"""
Supports REST actions specifically intended / formatted for use by DataTables
"""

from collections import Iterable

from config import db
from marshmallow import ValidationError
from models import Analysis, AnalysisSchema, Person, PersonSchema, Project, \
    ProjectSchema


# utility functions
def deconstruct_form_data(ids, body, valid_keys):
    """
    Format POST/PUT form data received from Datatables for easy consumption

    Formatting includes the following:
      - de-serialise data into a nested dict using "data{integer}" prefix from each key
      - use the first value of the array for each value
      - remove the 'action' key
      - replace empty string values with None
        (prevents db crashing when inserting "" for certain types such as date fields)

    - Expected body format::

         {
           'action': ['create'],
           'data0analysisname': ['RNAHyper customer complaint'],
           'data0analysisdescription': ['customer reported high rRNA rate'],
           'data0analysisdate': ['2018-03-01'],
           'data0analysisdepartment': ['CIR'],
           'data0analysisanalyst': ['Jack Viljoen'],
           'data0analysisproject_id': ['1']
         }

    - Return format::
        {
            0:
                {
                  "name": "RNAHyper customer complaint",
                  "description": "customer reported high rRNA rate",
                  "date": "2018-03-01",
                  "department": "CIR",
                  "analyst_id": "3",
                  "project_id": "1"
                }
        }

    :param ids: id of the row to modify or create
    :param body: form data from dataTables
    :param valid_keys: list of keys to "search" for in body
    :return: mapping of each valid key to it's corresponding value from the form data
    """
    # buffer against a single int vs list of ints
    if not isinstance(ids, Iterable):
        ids = [ids]

    # build the response
    responses = {}

    for i in ids:
        response = {}
        for k, v in body.items():
            if f"data{i}analysis" in k:
                key = k.replace(f"data{i}analysis", "")
                if key in valid_keys:
                    response[key] = v[0] if v[0] is not "" else None
                    responses[i] = response

    return responses


def format_error_response(error_response, table_name=""):
    """
    Format marshmallow validation errors for use in Datatables

    - Initial format of error_response::

        {
          'fieldErrors': [
            {
              'description': ['Not a valid email address.'],
              'date': ['Field may not be null.']
            }
          ]
        }

    - Return format::

        {
          "fieldErrors": [
            {
              "name": "first_name",
              "status": "This field is required"
            },
            {
              "name": "last_name",
              "status": "This field is required"
            }
          ]
        }

    :param error_response: nested dict describing field validation errors from Marshmallow
    :param table_name: name of the table to use when parsing table validation errors
    :return: nested dict describing the field validation errors in DataTables format
    """
    response = {"fieldErrors": []}

    for error in error_response["fieldErrors"]:
        for k, v in error.items():
            formatted_error = {"name": f"{table_name}.{k}", "status": v[0]}
            response["fieldErrors"].append(formatted_error)

    return response


def get_project_dropdown_options():
    """
    Build a list of project options for Datatables 'select' fields

    - Return format::

        [
          {
            "label": "RNA Hyper",
            "value": 1
          },
          {
            "label": "RNASeq v1",
            "value": 2
          },
          {
            "label": "Dual-index adapters",
            "value": 3
          }
        ]

    :return: list of dicts mapping project.name to project.id
    """
    projects = Project.query.all()
    project_schema = ProjectSchema(many=True)
    project_data = project_schema.dump(projects)
    project_options = []

    for p in project_data:
        p_formatted = {"label": p["name"], "value": p["id"]}
        project_options.append(p_formatted)

    return project_options


def get_analyst_dropdown_options():
    """
    Build a list of analyst options for Datatables 'select' fields

    - Return format::

        [
          {
            "label": "David",
            "value": 1
          },
          {
            "label": "Davis",
            "value": 2
          },
          {
            "label": "Jack",
            "value": 3
          }
        ]

    :return: list of dicts mapping analyst.name to analyst.id
    """
    analysts = Person.query.all()
    analyst_schema = PersonSchema(many=True)
    analyst_data = analyst_schema.dump(analysts)
    analyst_options = []

    for a in analyst_data:
        a_formatted = {"label": a["name"], "value": a["id"]}
        analyst_options.append(a_formatted)

    return analyst_options


# API endpoint actions
def read_all_analyses():
    """
    Responds to GET request for /api/datatables/analyses; returns all analysis data
    formatted for consumption by DataTables.

    - Expected success response::

        HTTP Status Code: 201

        {
          "data": [
            {
              "id": 4,
              "analysis": {
                "description": "Routine QC run for RNASeq v1 QC conducted by Eugene",
                "analyst": "Jack Viljoen",
                "department": "QC",
                "project_id": 2,
                "id": 4,
                "date": "2018-08-01",
                "name": "KAPA_MSQ_100"
              },
              "project": {
                "name": "RNASeq v1"
              }
            }
          ],
          "options": {
            "analysis.project_id": [
              {
                "label": "RNA Hyper",
                "value": 1
              },
              {
                "label": "RNASeq v1",
                "value": 2
              },
              {
                "label": "Dual-index adapters",
                "value": 3
              }
            ]
          }
        }

    :return: json string of analyses data; HTTP status code
    """
    # create the list of analyses from our data
    analyses = Analysis.query.order_by(Analysis.name).all()

    # serialize the data for the response
    analysis_schema = AnalysisSchema(many=True)
    analysis_data = analysis_schema.dump(analyses)

    # query the database and build the response for DT
    response = {
        "data": [],
        "options": {"analysis.project_id": [], "analysis.analyst_id": []},
    }

    for a in analysis_data:
        p = Project.query.get(a["project_id"])
        an = Person.query.get(a["analyst_id"])
        result = {
            "id": a["id"],
            "analysis": a,
            "project": {"name": p.name},
            "person": {"name": an.name},
        }
        response["data"].append(result)

    # add DT options for project, analyst
    project_options = get_project_dropdown_options()
    analyst_options = get_analyst_dropdown_options()
    response["options"]["analysis.project_id"] = project_options
    response["options"]["analysis.analyst_id"] = analyst_options

    # return the response
    return response, 201


def create_analyses(body):
    """
    Responds to POST request for /api/datatables/analyses; creates a new analysis
    using DataTables form data

    - Expected body format::

         {
           'action': ['create'],
           'data0analysisname': ['RNAHyper customer complaint again'],
           'data0analysisdescription': ['customer reported high rRNA rate'],
           'data0analysisdate': ['2018-03-01'],
           'data0analysisdepartment': ['CIR'],
           'data0analysisanalyst': ['Jack Viljoen'],
           'data0analysisproject_id': ['1']
         }

    - Expected success response::

        HTTP Status Code: 201
        {
          "data": [
            {
              "id": 4,
              "analysis": {
                "description": "Routine QC run for RNASeq v1 QC conducted by Eugene",
                "analyst": "Jack Viljoen",
                "department": "QC",
                "project_id": 2,
                "id": 4,
                "date": "2018-08-01",
                "name": "KAPA_MSQ_100"
              },
              "project": {
                "name": "RNASeq v1"
              }
            }
          ],
          "options": {
            "analysis.project_id": [
              {
                "label": "RNA Hyper",
                "value": 1
              },
              {
                "label": "RNASeq v1",
                "value": 2
              },
              {
                "label": "Dual-index adapters",
                "value": 3
              }
            ]
          }
        }

    - Expected Fail Response::

        HTTP Status Code: 409
        {
          "fieldErrors": [
            {
              "name": "field name",
              "status": "message"
            }
          ]
        }

    :param body: body content describing the new analysis; serialised to json by OpenAPI
    :return: json string of analysis data or error spec; HTTP status code
    """
    temp_id = 0
    valid_keys = [
        "name",
        "description",
        "date",
        "analyst_id",
        "department",
        "project_id",
    ]
    analysis = deconstruct_form_data(temp_id, body, valid_keys)
    error_response = {"fieldErrors": []}
    success_response = {
        "data": [],
        "options": {"analysis.project_id": [], "analysis.analyst_id": []},
    }

    # check if the analysis already exists
    existing_analysis = Analysis.query.filter(
        Analysis.name == analysis[temp_id]["name"]
    ).one_or_none()

    if existing_analysis is None:
        # load Analysis object and validate it's schema
        try:
            schema = AnalysisSchema()
            new_analysis = schema.load(analysis[temp_id], session=db.session)
        except ValidationError as err:
            error_response["fieldErrors"].append(err.messages)
        else:
            # add the analysis to the database
            db.session.add(new_analysis)
            db.session.commit()

            # serialize the new analysis and project data
            a = schema.dump(new_analysis)
            p = Project.query.get(a["project_id"])
            an = Person.query.get(a["analyst_id"])

            # format the data and add it to the response
            result = {
                "id": a["id"],
                "analysis": a,
                "project": {"name": p.name},
                "person": {"name": an.name},
            }
            success_response["data"].append(result)
    else:
        # analysis already exists
        error = {"name": ["Field must be unique"]}
        error_response["fieldErrors"].append(error)

    # return the appropriate response
    if not error_response["fieldErrors"]:
        # add DT options for project, analyst
        project_options = get_project_dropdown_options()
        analyst_options = get_analyst_dropdown_options()
        success_response["options"]["analysis.project_id"] = project_options
        success_response["options"]["analysis.analyst_id"] = analyst_options

        return success_response, 200
    else:
        error_response = format_error_response(error_response, table_name="analysis")
        return error_response, 409


def update_analysis(id, body):
    """
    Responds to PUT request for /api/datatables/analyses; updates existing analysis
    using DataTables form data and analysis id

    - Expected body format::

         {
           'action': ['edit'],
           'data0analysisname': ['RNAHyper customer complaint again'],
           'data0analysisdescription': ['customer reported high rRNA rate'],
           'data0analysisdate': ['2018-03-01'],
           'data0analysisdepartment': ['CIR'],
           'data0analysisanalyst': ['Jack Viljoen'],
           'data0analysisproject_id': ['1']
         }

    - Expected success response::

        HTTP Status Code: 200
        {
          "data": [
            {
              "id": 4,
              "analysis": {
                "description": "Routine QC run for RNASeq v1 QC conducted by Eugene",
                "analyst": "Jack Viljoen",
                "department": "QC",
                "project_id": 2,
                "id": 4,
                "date": "2018-08-01",
                "name": "KAPA_MSQ_100"
              },
              "project": {
                "name": "RNASeq v1"
              }
            }
          ],
          "options": {
            "analysis.project_id": [
              {
                "label": "RNA Hyper",
                "value": 1
              },
              {
                "label": "RNASeq v1",
                "value": 2
              },
              {
                "label": "Dual-index adapters",
                "value": 3
              }
            ]
          }
        }

    - Expected Fail Response::

        HTTP Status Code: 409
        {
          "fieldErrors": [
            {
              "name": "field name",
              "status": "message"
            }
          ]
        }

    :param id: list of analysis ids to update
    :param body: body content describing the new analysis information; serialised to json by OpenAPI
    :return: json string of analysis data or error spec; HTTP status code
    """
    # ToDo: 404 on analyses does not exist
    valid_keys = [
        "name",
        "description",
        "date",
        "analyst_id",
        "department",
        "project_id",
    ]
    analyses = deconstruct_form_data(id, body, valid_keys)
    error_response = {"fieldErrors": []}
    success_response = {
        "data": [],
        "options": {"analysis.project_id": [], "analysis.analyst_id": []},
    }

    # update and then validate each new analysis schema
    for k, v in analyses.items():
        initial = Analysis.query.filter(Analysis.id == k).one_or_none()

        if initial:
            duplicate_name = (
                Analysis.query.filter(Analysis.name == v.get("name"))
                .filter(Analysis.id != k)
                .one_or_none()
            )

            if not duplicate_name:
                # convert current Analysis object to a dict
                schema = AnalysisSchema()
                analysis_dict = schema.dump(initial)

                # update Analysis dict with new values
                analysis_dict.update(v)

                # validate schema by loading Analysis object
                try:
                    updated = schema.load(analysis_dict)
                except ValidationError as err:
                    error_response["fieldErrors"].append(err.messages)
                else:
                    # merge the new obj into the persistent obj and commit changes
                    db.session.merge(updated)
                    db.session.commit()

                    # serialize the updated analysis and project data
                    a = schema.dump(updated)
                    p = Project.query.get(a["project_id"])
                    an = Person.query.get(a["analyst_id"])

                    # format the data and add it to the response
                    result = {
                        "id": a["id"],
                        "analysis": a,
                        "project": {"name": p.name},
                        "person": {"name": an.name},
                    }
                    success_response["data"].append(result)
            else:
                error = {"name": ["Field must be unique"]}
                error_response["fieldErrors"].append(error)
        else:
            error = f"id {k} not found"
            error_response["error"] = error
            return error_response, 404

    if not error_response["fieldErrors"]:
        # add DT options for project, analyst
        project_options = get_project_dropdown_options()
        analyst_options = get_analyst_dropdown_options()
        success_response["options"]["analysis.project_id"] = project_options
        success_response["options"]["analysis.analyst_id"] = analyst_options

        return success_response, 200
    else:
        error_response = format_error_response(error_response, table_name="analysis")
        return error_response, 409


def delete_analysis(id):
    """
    Responds to DELETE request for /api/datatables/analyses by Data Tables.
    Removes an existing analysis by its analysis id.

    - Expected success response::

        HTTP Status Code: 200
        {}

    - Expected success response::

        HTTP Status Code: 400
        {
          "fieldErrors": [
            {
              "name": "field name",
              "status": "message"
            }
          ]
        }

    :param id: list of analysis ids to delete
    :return: json string of analysis data or error spec; HTTP status code
    """
    field_errors = []

    for i in id:
        # Get the analysis requested
        analysis = Analysis.query.filter(Analysis.id == i).one_or_none()

        # Did we find an analysis?
        if analysis is not None:
            db.session.delete(analysis)
            db.session.commit()
        else:
            error = {"something": "some message"}  # ToDo: align with DT field format
            field_errors.append(error)

    if not field_errors:
        return {}, 200
    else:
        return field_errors, 404
