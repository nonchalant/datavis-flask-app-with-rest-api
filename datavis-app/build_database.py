import os
from datetime import datetime

from .config import db, db_name
from .models import Person, Project, Analysis, Sample, Result

# Data to initialize database with
PEOPLE = [
    {"name": "David", "surname": "Penkler"},
    {"name": "Davis", "surname": "Todt"},
    {"name": "Jack", "surname": "Viljoen"},
    {"name": "du Toit", "surname": "Schabort"},
    {"name": "Michael", "surname": "Berry"},
    {"name": "Ruben", "surname": "van der Merwe"},
]

PROJECTS = [
    {"name": "RNA Hyper", "path": "/sc1/groups/cape-town/projects/RNAHyper"},
    {"name": "RNASeq v1", "path": "/sc1/groups/cape-town/projects/RNASeqV1"},
    {
        "name": "Dual-index adapters",
        "path": "/sc1/groups/cape-town/projects/DI-Adapters",
    },
    {
        "name": "Dual-index adapters (raw materials)",
        "path": "/sc1/groups/cape-town/projects/DI-Adapters",
    },
    {
        "name": "Single-index adapters",
        "path": "/sc1/groups/cape-town/projects/SI-Adapters",
    },
    {"name": "UDI adapters", "path": "/sc1/groups/cape-town/projects/UDI-adapters"},
    {"name": "GlobinErase", "path": "/sc1/groups/cape-town/projects/Globin"},
    {"name": "RiboErase", "path": "/sc1/groups/cape-town/projects/RiboErase"},
    {"name": "HiFi", "path": "/sc1/groups/cape-town/projects/HiFi_Upscale"},
    {"name": "Kapa Pure Beads", "path": "/sc1/groups/cape-town/projects/PureBeads_v2"},
    {"name": "xPETE", "path": "/sc1/groups/cape-town/projects/xPETE"},
    {"name": "Sterling", "path": "/sc1/groups/cape-town/projects/Sterling"},
    {"name": "Smiley", "path": "/sc1/groups/cape-town/projects/Smiley"},
    {"name": "NUPETE", "path": "/sc1/groups/cape-town/projects/NUPETE"},
    {"name": "HyperPlus", "path": "/sc1/groups/cape-town/projects/HyperPlus"},
    {"name": "HyperRefresh", "path": "/sc1/groups/cape-town/projects/HyperRefresh"},
]

ANALYSES = [
    {
        "name": "Product de-risking",
        "description": "Pre-v&v study to 'de-risk' the product since no sequencing has been performed yet",
        "department": "Dev",
        "date": datetime(2018, 12, 1).date(),
        "analyst_id": 2,
        "project_id": 10,
    },
    {
        "name": "RNAHyper customer complaint",
        "description": "customer reported high rRNA rate",
        "department": "CIR",
        "date": datetime(2018, 3, 1).date(),
        "analyst_id": 3,
        "project_id": 1,
    },
    {
        "name": "RNAHyper verification data",
        "description": "Verification data for RNAHyper as part of the PDP, as conducted in 2015",
        "department": "Dev",
        "date": datetime(2018, 8, 1).date(),
        "analyst_id": 2,
        "project_id": 1,
    },
    {
        "name": "KAPA_MSQ_100",
        "description": "Routine QC run for RNASeq v1 QC conducted by Eugene",
        "department": "QC",
        "date": datetime(2018, 8, 1).date(),
        "analyst_id": 3,
        "project_id": 2,
    },
    {
        "name": "KAPA_MSQ_111",
        "description": "Routine QC run for RNASeq v1 QC conducted by Eugene",
        "department": "QC",
        "date": datetime(2018, 11, 1).date(),
        "analyst_id": 2,
        "project_id": 2,
    },
    {
        "name": "KAPA_MSQ_120",
        "description": "Routine QC run for RNASeq v1 QC conducted by not Eugene",
        "department": "QC",
        "date": datetime(2018, 12, 1).date(),
        "analyst_id": 2,
        "project_id": 2,
    },
]

SAMPLES = [
    {
        "name": "S01",
        "sample_metadata": "{'kit': 'RNAHyper', 'enrichment': 'riboerase'}",
        "analysis_id": 2,
    },
    {
        "name": "S02",
        "sample_metadata": "{'kit': 'competitor', 'enrichment': 'mRNA Capture'}",
        "analysis_id": 2,
    },
    {
        "name": "JHE_01",
        "sample_metadata": "{'kit': 'HyperPrep', 'enrichment': 'N/A'}",
        "analysis_id": 1,
    },
    {
        "name": "JHE_02",
        "sample_metadata": "{'kit': 'HyperPrep', 'enrichment': 'N/A'}",
        "analysis_id": 1,
    },
    {
        "name": "Kapa_RNAHyper_01",
        "sample_metadata": "{'kit': 'RNAHyper', 'enrichment': 'riboerase'}",
        "analysis_id": 3,
    },
    {
        "name": "Kapa_RNAHyper_02",
        "sample_metadata": "{'kit': 'RNAHyper', 'enrichment': 'riboerase'}",
        "analysis_id": 3,
    },
    {
        "name": "Kapa_RNAHyper_03",
        "sample_metadata": "{'kit': 'RNAHyper', 'enrichment': 'riboerase'}",
        "analysis_id": 3,
    },
    {
        "name": "REF_01",
        "sample_metadata": "{'kit': 'RNASeqV1', 'enrichment': 'riboerase'}",
        "analysis_id": 4,
    },
    {
        "name": "TEST_01",
        "sample_metadata": "{'kit': 'RNASeqV1', 'enrichment': 'riboerase'}",
        "analysis_id": 4,
    },
    {
        "name": "TEST_02",
        "sample_metadata": "{'kit': 'RNASeqV1', 'enrichment': 'riboerase'}",
        "analysis_id": 4,
    },
    {
        "name": "REF_01",
        "sample_metadata": "{'kit': 'RNASeqV1', 'enrichment': 'riboerase'}",
        "analysis_id": 5,
    },
    {
        "name": "TEST_01",
        "sample_metadata": "{'kit': 'RNASeqV1', 'enrichment': 'riboerase'}",
        "analysis_id": 5,
    },
    {
        "name": "TEST_03",
        "sample_metadata": "{'kit': 'RNASeqV1', 'enrichment': 'riboerase'}",
        "analysis_id": 5,
    },
    {
        "name": "REF_02",
        "sample_metadata": "{'kit': 'RNASeqV1', 'enrichment': 'riboerase'}",
        "analysis_id": 6,
    },
    {
        "name": "TEST_03",
        "sample_metadata": "{'kit': 'RNASeqV1', 'enrichment': 'riboerase'}",
        "analysis_id": 6,
    },
]
RESULTS = [
    {"metrics": "{'mapping_rate': 0.945, 'residual_rRNA_%': 6.23}", "sample_id": 1},
    {"metrics": "{'mapping_rate': 0.925, 'residual_rRNA_%': 4.91}", "sample_id": 2},
    {"metrics": "{'mapping_rate': 0.99, 'residual_rRNA_%': 'N/A'}", "sample_id": 3},
    {"metrics": "{'mapping_rate': 0.98, 'residual_rRNA_%': 'N/A'}", "sample_id": 4},
    {"metrics": "{'mapping_rate': 0.970, 'residual_rRNA_%': 1.32}", "sample_id": 5},
    {"metrics": "{'mapping_rate': 0.954, 'residual_rRNA_%': 1.05}", "sample_id": 6},
    {"metrics": "{'mapping_rate': 0.959, 'residual_rRNA_%': 0.97}", "sample_id": 7},
    {"metrics": "{'mapping_rate': 0.987, 'residual_rRNA_%': 4.68}", "sample_id": 8},
    {"metrics": "{'mapping_rate': 0.959, 'residual_rRNA_%': 3.91}", "sample_id": 9},
    {"metrics": "{'mapping_rate': 0.950, 'residual_rRNA_%': 4.45}", "sample_id": 10},
    {"metrics": "{'mapping_rate': 0.962, 'residual_rRNA_%': 2.20}", "sample_id": 11},
    {"metrics": "{'mapping_rate': 0.981, 'residual_rRNA_%': 1.98}", "sample_id": 12},
    {"metrics": "{'mapping_rate': 0.968, 'residual_rRNA_%': 2.76}", "sample_id": 13},
    {"metrics": "{'mapping_rate': 0.966, 'residual_rRNA_%': 1.68}", "sample_id": 14},
    {"metrics": "{'mapping_rate': 0.970, 'residual_rRNA_%': 2.96}", "sample_id": 15},
]

# Delete database file if it exists currently
if os.path.exists(db_name):
    os.remove(db_name)

# Create the database
db.create_all()

# iterate over the PEOPLE structure and populate the database
for person in PEOPLE:
    p = Person(name=person.get("name"), surname=person.get("surname"))
    db.session.add(p)

# iterate over the PROJECTS structure and populate the database
for project in PROJECTS:
    p = Project(name=project.get("name"), path=project.get("path"))
    db.session.add(p)

# iterate over the ANALYSES structure and populate the database
for analysis in ANALYSES:
    a = Analysis(
        name=analysis.get("name"),
        description=analysis.get("description"),
        department=analysis.get("department"),
        date=analysis.get("date"),
        analyst_id=analysis.get("analyst_id"),
        project_id=analysis.get("project_id"),
    )
    db.session.add(a)

# iterate over the SAMPLES structure and populate the database
for sample in SAMPLES:
    s = Sample(
        name=sample.get("name"),
        sample_metadata=sample.get("sample_metadata"),
        analysis_id=sample.get("analysis_id"),
    )
    db.session.add(s)

# iterate over the RESULTS structure and populate the database
for result in RESULTS:
    r = Result(metrics=result.get("metrics"), sample_id=result.get("sample_id"))
    db.session.add(r)

db.session.commit()
