/*
 * JavaScript file for the application to demonstrate
 * using the API
 */

// Create the namespace instance
let ns = {};

// Create the model instance
ns.model = (function () {
    'use strict';

    let $event_pump = $('body');

    // Return the API
    return {
        'read_people': function () {
            let ajax_options = {
                type: 'GET',
                url: 'api/people',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_people', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        'read_projects': function () {
            let ajax_options = {
                type: 'GET',
                url: 'api/projects',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_projects', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        'read_analyses': function () {
            let ajax_options = {
                type: 'GET',
                url: 'api/analyses',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_analyses', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        'read_samples': function () {
            let ajax_options = {
                type: 'GET',
                url: 'api/samples',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_samples', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        'fetch_metadata': function (api_filter_str) {
            let ajax_options = {
                type: 'GET',
                url: 'api/samples/metadata' + api_filter_str,
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_metadata', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        'fetch_available_metrics': function (api_filter_str) {
            let ajax_options = {
                type: 'GET',
                url: 'api/results/metrics' + api_filter_str,
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_metrics', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },

        'read_combined_sample_data': function (api_filter_str) {
            let ajax_options = {
                type: 'GET',
                url: 'api/samples/combine_and_filter' + api_filter_str,
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_combined_sample_data', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        'fetch_vega_plot_spec': function (api_filter_str) {
            let ajax_options = {
                type: 'GET',
                url: 'api/results/plot_data' + api_filter_str,
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_fetch_vega_plot_spec', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        'fetch_dashboard_plot_spec': function (api_filter_str) {
            let ajax_options = {
                type: 'GET',
                url: 'api/results/plot_dashboard',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_fetch_dashboard_plot_spec', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        'read_results': function () {
            let ajax_options = {
                type: 'GET',
                url: 'api/results',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_read_success_results', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        create: function (person) {
            let ajax_options = {
                type: 'POST',
                url: 'api/people',
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(person)
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_create_success', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        update: function (person) {
            let ajax_options = {
                type: 'PUT',
                url: `api/people/${person.person_id}`,
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(person)
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_update_success', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        },
        'delete': function (person_id) {
            let ajax_options = {
                type: 'DELETE',
                url: `api/people/${person_id}`,
                accepts: 'application/json',
                contentType: 'plain/text'
            };
            $.ajax(ajax_options)
                .done(function (data) {
                    $event_pump.trigger('model_delete_success', [data]);
                })
                .fail(function (xhr, textStatus, errorThrown) {
                    $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
                })
        }
    };
}());

// Create the view instance
ns.view = function () {
    'use strict';

    let $person_id = $('#person_id'),
        $name = $('#name'),
        $surname = $('#surname');

    // return the API
    return {
        reset: function () {
            $person_id.val('');
            $surname.val('');
            $name.val('').focus();
        },
        update_editor: function (person) {
            $person_id.val(person.person_id);
            $surname.val(person.surname);
            $name.val(person.name).focus();
        },
        build_table_people: function (people) {
            let columns = [];

            for (let i = 0, l = Object.keys(people[0]).length; i < l; i++) {
                let col = Object.keys(people[0])[i];  // grab the key name; e.g. "Sample"
                columns.push({"title": col, "data": col});  // this format is needed for dynamic columns
            }
            ;

//            $("#people-table").DataTable({
//                "data": people,
//                "columns": columns
//            });

        },

        build_table_projects: function (projects) {
            let columns = [];

            for (let i = 0, l = Object.keys(projects[0]).length; i < l; i++) {
                let col = Object.keys(projects[0])[i];  // grab the key name; e.g. "Sample"
                columns.push({"title": col, "data": col});  // this format is needed for dynamic columns
            }
            ;

            $("#projects-table").DataTable({
                "data": projects,
                "columns": columns
            });

        },
        populate_projects_search: function (projects_data) {
            let options = '';
            let uniq_projects = new Set();

            // loop through all projects and add to options list in dropdown
            for (let i = 0, l = projects_data.length; i < l; i++) {
                if (!uniq_projects.has(projects_data[i].name)) {
                    options += `<option value="${projects_data[i].id}">${projects_data[i].name}</option>`;
                    uniq_projects.add(projects_data[i].name);
                }
            }

            $('#project-selection-dropdown').append(options);

        },
        populate_analyses_search: function (analyses_data) {
            let options = '';
            let uniq_analyses = new Set();
            let old_option_ids = new Set();
            let old_option_names = new Set();
            let old_selection_ids = new Set();
            let old_selection_names = new Set();

            let analysis_dropdown_elem = $('#analysis-selection-dropdown').find('option');
            let analysis_selected_elem = $('#analysis-selection-dropdown').parent().find('a');

            // build a list of the options currently present before updating the DOM
            for (let i = 0, l = analysis_dropdown_elem.length; i < l; i++) {
                let old_option_id = Number($(analysis_dropdown_elem[i]).attr('value'));
                let old_option_name = $(analysis_dropdown_elem[i]).html();
                old_option_ids.add(old_option_id);
                old_option_names.add(old_option_name);
            }

            // build a list of the selections currently present before updating the DOM
            for (let i = 0, l = analysis_selected_elem.length; i < l; i++) {
                let old_selection_id = Number($(analysis_selected_elem[i]).attr('data-value'));
                let old_selection_name = $(analysis_selected_elem[i]).innerText;
                old_selection_ids.add(old_selection_id);
                old_selection_names.add(old_selection_name);
            }

            // create list of unique analyses and add *new* options to dropdown if not already present
            for (let i = 0, l = analyses_data.length; i < l; i++) {
                if (!uniq_analyses.has(analyses_data[i].id)) {
                    uniq_analyses.add(analyses_data[i].id);
                    if (!old_option_ids.has(analyses_data[i].id)) {
                        options += `<option value="${analyses_data[i].id}">${analyses_data[i].name}</option>`;
                    }
                    // if the *id* already exists but the analysis *name* was renamed
                    if (old_option_ids.has(analyses_data[i].id) && !old_option_names.has(analyses_data[i].name)) {
                        $($('#analysis-selection-dropdown').find(`option[value=${analyses_data[i].id}]`)).text(analyses_data[i].name);
                        $($('#analysis-selection-dropdown').parent().find('div.menu')).find(`div.item[data-value=${analyses_data[i].id}]`).text(analyses_data[i].name);
                    }
                    // if the *id* was previously selected but the analysis *name* was renamed
                    if (old_selection_ids.has(analyses_data[i].id) && !old_selection_names.has(analyses_data[i].name)) {
                        $('#analysis-selection-dropdown').parent().find(`a[data-value=${analyses_data[i].id}]`)[0].childNodes[0].nodeValue = analyses_data[i].name;
                    }
                }

            }

            // remove old selections from DOM
            old_selection_ids.forEach(function (i) {
                if (!uniq_analyses.has(i)) {
                    $('#analysis-selection-dropdown').parent().find(`a[data-value=${i}]`).remove();
                }
            })

            // remove old options from DOM
            old_option_ids.forEach(function (i) {
                if (!uniq_analyses.has(i)) {
                    $('#analysis-selection-dropdown').find(`option[value=${i}]`).remove();
                }
            })

            // add new options to DOM
            $('#analysis-selection-dropdown').append(options);

        },
        populate_departments_search: function (departments_data) {
            let options = '';
            let uniq_departments = new Set();
            let old_options = new Set();
            let old_selections = new Set();

            let department_dropdown_elem = $('#department-selection-dropdown').find('option');
            let department_selected_elem = $('#department-selection-dropdown').parent().find('a');

            // build a list of the options currently present before updating the DOM
            for (let i = 0, l = department_dropdown_elem.length; i < l; i++) {
                let old_option = $(department_dropdown_elem[i]).attr('value');
                old_options.add(old_option);
            }

            // build a list of the selections currently present before updating the DOM
            for (let i = 0, l = department_selected_elem.length; i < l; i++) {
                let old_selection = $(department_selected_elem[i]).attr('data-value');
                old_selections.add(old_selection);
            }

            // create list of unique departments and add *new* options to dropdown if not already present
            for (let i = 0, l = departments_data.length; i < l; i++) {
                if (!uniq_departments.has(departments_data[i].department)) {
                    uniq_departments.add(departments_data[i].department);
                    if (!old_options.has(departments_data[i].department)) {
                        options += `<option value="${departments_data[i].department}">${departments_data[i].department}</option>`;
                    }
                }
            }

            // remove old selections from DOM
            old_selections.forEach(function (i) {
                if (!uniq_departments.has(i)) {
                    $('#department-selection-dropdown').parent().find(`a[data-value=${i}]`).remove();
                }
            })

            // remove old options from DOM
            old_options.forEach(function (i) {
                if (!uniq_departments.has(i) && i != "") {
                    $('#department-selection-dropdown').find(`option[value=${i}]`).remove();
                }
            })

            // add new options to DOM
            $('#department-selection-dropdown').append(options);

        },
        populate_analysts_search: function (analysts_data) {
            let options = '';
            let uniq_analysts = new Set();
            let old_options = new Set();
            let old_selections = new Set();

            let analyst_dropdown_elem = $('#analyst-selection-dropdown').find('option');
            let analyst_selected_elem = $('#analyst-selection-dropdown').parent().find('a');

            // build a list of the options currently present before updating the DOM
            for (let i = 0, l = analyst_dropdown_elem.length; i < l; i++) {
                let old_option = Number($(analyst_dropdown_elem[i]).attr('value'));
                old_options.add(old_option);
            }

            // build a list of the selections currently present before updating the DOM
            for (let i = 0, l = analyst_selected_elem.length; i < l; i++) {
                let old_selection = Number($(analyst_selected_elem[i]).attr('data-value'));
                old_selections.add(old_selection);
            }

            // create list of unique analysts and add *new* options to dropdown if not already present
            for (let i = 0, l = analysts_data.length; i < l; i++) {
                if (!uniq_analysts.has(analysts_data[i].analyst_id)) {
                    uniq_analysts.add(analysts_data[i].analyst_id);
                    if (!old_options.has(analysts_data[i].analyst_id)) {
                        options += `<option value="${analysts_data[i].analyst_id}">${analysts_data[i].analyst_name}</option>`;
                    }
                }
            }

            // remove old selections from DOM
            old_selections.forEach(function (i) {
                if (!uniq_analysts.has(i)) {
                    $('#analyst-selection-dropdown').parent().find(`a[data-value=${i}]`).remove();
                }
            })

            // remove old options from DOM
            old_options.forEach(function (i) {
                if (!uniq_analysts.has(i)) {
                    $('#analyst-selection-dropdown').find(`option[value=${i}]`).remove();
                }
            })

            // add new options to DOM
            $('#analyst-selection-dropdown').append(options);

        },
        build_table_analyses: function (analyses) {
            var editor; // use a global for the submit and return data rendering in the examples

            editor = new $.fn.dataTable.Editor({
                ajax: {
                    create: {
                        type: 'POST',
                        url: 'api/datatables/analyses',
                    },
                    edit: {
                        type: 'PUT',
                        url: 'api/datatables/analyses?id=_id_'
                    },
                    remove: {
                        type: 'DELETE',
                        url: 'api/datatables/analyses?id=_id_'
                    }
                },
                table: "#analyses-table",
                idSrc: 'id',
                fields: [{
                    label: "Analysis name:",
                    name: "analysis.name",
                    type: "text",
                    multiEditable: false
                },
                    {
                        label: "Analysis description:",
                        name: "analysis.description",
                        type: "textarea",
                    },
                    {
                        label: "Date:",
                        name: "analysis.date",
                        type: "date"
                    },
                    {
                        label: "Department:",
                        name: "analysis.department"
                    },
                    {
                        label: "Analyst:",
                        name: "analysis.analyst_id",
                        type: "select",
                        placeholder: "select an analyst"
                        //type: "select",
                        //options: ["DM Todt", "Other"]
                        // placeholder: "select an analyst"
                    },
                    {
                        label: "Project:",
                        name: "analysis.project_id",
                        type: "select",
                        placeholder: "select a project"
                    }
                ]
            });

            // Activate an inline edit on click of a table cell
            $('#analyses-table').on('click', 'tbody td:not(:first-child)', function (e) {
                editor.inline(this);
            });

//            editor.field( 'analysis.description' ).input().on( 'change', function () {
//              editor.submit();
//            } );

            $("#analyses-table").DataTable({
                ajax: {type: "GET", url: "api/datatables/analyses"},
                scrollX: false,
                columns: [
                    {data: "id", title: "id"},
                    {data: "analysis.name", title: "Analysis name"},
                    {data: "analysis.description", title: "Description", editField: "analysis.description"},
                    {data: "analysis.date", title: "Date"},
                    {data: "analysis.department", title: "Department"},
                    {data: "person.name", title: "Analyst", editField: "analysis.analyst_id"},
                    {data: "project.name", title: "Project", editField: "analysis.project_id"}
                ],
                dom: "Bfrtip",
                select: true,
                buttons: [
                    {extend: "create", editor: editor},
                    {extend: "edit", editor: editor},
                    {extend: "remove", editor: editor}
                ]
            });

        },
        build_table_samples: function (samples) {
            let columns = [];

            for (let i = 0, l = Object.keys(samples[0]).length; i < l; i++) {
                let col = Object.keys(samples[0])[i];  // grab the key name; e.g. "Sample"
                columns.push({"title": col, "data": col});  // this format is needed for dynamic columns
            }
            ;

            $("#samples-table").DataTable({
                "data": samples,
                "columns": columns
            });

        },
        build_table_results: function (results) {
            let columns = [];

            for (let i = 0, l = Object.keys(results[0]).length; i < l; i++) {
                let col = Object.keys(results[0])[i];  // grab the key name; e.g. "Sample"
                columns.push({"title": col, "data": col});  // this format is needed for dynamic columns
            }
            ;

            $("#results-table").DataTable({
                "data": results,
                "columns": columns,
            });

        },
        build_search_results_table: function (results, remove_selected_samples) {
            let columns = [{'title': 'remove', 'data': 'checkbox-row', 'className': "select-checkbox"}];
            let data = [];

            // clear the current table from the DOM and create a new one
            let table_element = '<table id="sample-search-results-table" class="ui celled table" width="100%"></table>';
            $('#sample-search-results').empty();  // remove all children from this div
            $('#sample-search-results').append(table_element);

            if (results.length > 0 && results != 'none') {
                for (let i = 0, l = results.length; i < l; i++) {
                    let result = results[i];
                    result['checkbox-row'] = '';
                    data.push(result);
                }

                for (let i = 0, l = Object.keys(results[0]).length; i < l; i++) {
                    let col = Object.keys(results[0])[i];  // grab the key name; e.g. "Sample"
                    if (col != 'checkbox-row') {
                        columns.push({"title": col, "data": col});  // this format is needed for dynamic columns
                    }
                }
                ;

                // render the dynamic table
                var table = $('#sample-search-results-table').DataTable({
                    data: data,
                    columns: columns,
                    dom: '<"ui grid"<"twelve wide fluid column"l>>Bfrtip',
                    select: {
                        style: 'os',
                        selector: 'td:first-child'
                    },
                    order: [[3, 'asc']],
                    buttons: [
                        {
                            text: 'Mark all',
                            action: function () {
                                table.rows().select();
                            }
                        },
                        {
                            text: 'Mark none',
                            action: function () {
                                table.rows().deselect();
                            }
                        },
                        {
                            text: 'Remove marked samples',
                            action: function () {
                                let selected_rows = table.rows({selected: true}).data()
                                remove_selected_samples(selected_rows);
                            }
                        }
                    ]
                });

            } else {
                // if no results are present then create a default response
                data = [{'id': '-', 'Sample': 'No results; consider modifying the filter parameters'}];
                columns = [{'title': 'id', 'data': 'id'}, {'title': 'Sample', 'data': 'Sample'}];

                // render the table
                $('#sample-search-results-table').DataTable({
                    data: data,
                    columns: columns
                });

            }
            ;

        },

        update_basket: function (numberSamples, isChanged) {
            let animationName = 'animated pulse';
            let animationEnd = 'animationend oAnimationEnd mozAnimationEnd MSAnimationEnd webkitAnimationEnd';

            if (isChanged) {
                $('#samples-in-basket-value').html('Selected: ' + numberSamples);
                $('#samples-in-basket-value').parent().addClass(animationName).one(animationEnd, function () {
                    $(this).removeClass(animationName);
                });
            }
            ;
        },

        add_new_metadata_filter: function (metadata_id_counter) {
            let new_filter = `
            <div class="ui floating message metadata-group-message">
              <i class="close icon"></i>
              <div class="three fields metadata-group" data-group-id="${metadata_id_counter}"
                    id='meta-field-group-${metadata_id_counter}'>
                        <div class="six wide field">
                            <label>Metadata field</label>
                            <select class="ui search selection dropdown meta-value" id="metadata-field-${metadata_id_counter}">
                                <option value="">Select a field to filter on</option>
                            </select>
                        </div>

                        <div class="six wide field">
                            <label>Filter method</label>
                            <select id="filter-method-field-${metadata_id_counter}" class="ui selection dropdown meta-value">
                                <option value="">Select a filtering method</option>
                                <option value="greater-than">is greater than</option>
                                <option value="less-than">is less than</option>
                                <option value="equal-to">is equal to</option>
                                <option value="contains">contains the text</option>
                                <option value="does not contain">does not contain the text</option>
                            </select>
                        </div>
                        <div class="six wide field">
                            <label>Criteria</label>
                            <input class="meta-value" id="metadata-criteria-field-${metadata_id_counter}" type="text" name="first-name" placeholder="Enter a value to filter on">

                            </select>
                        </div>

                    </div>
            </div>
            `;
            $(new_filter).insertBefore($('#add-new-metadata-filter-button').parent().parent());
            $('select.dropdown').dropdown();

        },
        add_new_plotting_fields: function (available_metrics, available_metadata) {
            // remove current field set to replace with new
            $('.plotting-group-message').remove();

            // build new DOM element for plotting field group
            let options = '',
                new_plotting_field = `
                        <div class="ui floating message plotting-group-message">
                            <div class="three fields plotting-group" id='plotting-field-group'>
                                <div class="six wide field">
                                    <label>Metric name</label>
                                    <select class="ui search selection dropdown metric-name-value"
                                            id="plotting-field-0">
                                        <option value="">Select a metric to plot</option>`

            // loop through all metrics and add to options list in dropdown
            for (let i = 0, l = available_metrics.length; i < l; i++) {
                options += `<option value="${available_metrics[i]}">${available_metrics[i]}</option>`;
            }

            new_plotting_field += options;
            options = '';

            new_plotting_field += `</select>
                                </div>
                                <div class="six wide field">
                                    <label>Plot type</label>
                                    <select id="plotting-field-1"
                                            class="ui search selection dropdown metric-name-value">
                                        <option value="">Select the type of plot to create</option>
                                        <option value="strip-plot">Strip plot</option>
                                        <option value="bar-plot">Bar plot</option>
                                        <option value="line-plot">Line plot</option>
                                    </select>
                                </div>
                                <div class="six wide field">
                                    <label>Group by category</label>
                                    <select id="plotting-field-2"
                                            class="ui search selection dropdown metric-name-value">
                                        <option value="">Select a category for grouping</option>`

            // loop through all metadata and add to options list in dropdown
            for (let i = 0, l = available_metadata.length; i < l; i++) {
                options += `<option value="${available_metadata[i]}">${available_metadata[i]}</option>`;
            }

            new_plotting_field += options;
            options = '';
            new_plotting_field += `</select>
                                </div>
                            </div>
                        </div>`;

            // insert element into DOM before 'add new graph' button
            $(new_plotting_field).insertBefore($('#add-new-graph-button').parent().parent());
            $('select.dropdown').dropdown();

        },

        populate_metadata_filter: function (metadata_id_counter, available_metadata) {

            let options = '';
            let meta_field_id = "#metadata-field-" + metadata_id_counter;

            // loop through all metadata and add to options list in dropdown
            for (let i = 0, l = available_metadata.length; i < l; i++) {
                options += `<option value="${available_metadata[i]}">${available_metadata[i]}</option>`;
            }

            $(meta_field_id).append(options);
            $(meta_field_id).show();
        },
        plot_graphs: function (arrPlots) {
            let plotsPresent = false;

            if (arrPlots.length > 0) {
                $('#vis').empty();
                $('#vis').show();
                $('#vis').addClass('active').removeClass('out');

                for (let i = 0, j = arrPlots.length; i < j; i++) {
                    if (Object.keys(arrPlots[i]).length > 0) {
                        // at least one plot exists
                        plotsPresent = true;

                        // build the template element
                        let new_elem = `
                            <div class="ui fluid image floating message graph-message">
                              <i class="close icon"></i>
                              <div data-plot-id="${i}" id="vis-${i}"></div>
                            </div>`;

                        // embed the graph
                        $('#vis').append(new_elem);
                        vegaEmbed(`#vis-${i}`, arrPlots[i]).catch(console.error);

                        // fade in graph
                        $(`#vis-${i}`).addClass('active');
                    }

                }
            }
            if (plotsPresent) {
                // make success message visible
                $('#plot-fields-success-msg').addClass('active');

                setTimeout(function () {
                    $('#plot-fields-success-msg').addClass('out').removeClass('active');
                    setTimeout(function () {
                        $('#plot-fields-success-msg').removeClass('out');
                    }, 500); //Same time as animation
                }, 5000)

                // fade out placeholder graph
                $('#vis-placeholder').removeClass('active').addClass('out');
                setTimeout(function () {
                    $('#vis-placeholder').hide();
                }, 500); //Same time as animation
            } else {
                // fade in placeholder graph
                $('#vis-placeholder').show();
                $('#vis-placeholder').removeClass('out').addClass('active');

                // fade out graph
                $('#vis').removeClass('active').addClass('out');
                setTimeout(function () {
                    $('#vis').hide();
                }, 500); //Same time as animation

                // Todo: consider make plotting error message about empty plot specs visible
                //     : this may be useful in situations where the plotting failed

            }

        },
        create_dashboard_plot: function (specs) {
            // build the template element
            let new_elem = `
                <div class="ui segment" style="min-height: 375px;">
                  <div data-dash-plot-id="1" id="dash-1"></div>
                </div>`;

            // embed the graph
            $('#dash-plot').append(new_elem);
            vegaEmbed("#dash-1", specs[0]).catch(console.error);

            // build the template element
            new_elem = `
                <div class="ui segment" style="min-height: 375px;">
                  <div data-dash-plot-id="2" id="dash-2"></div>
                </div>`;

            // embed the graph
            $('#dash-plot-2').append(new_elem);
            vegaEmbed("#dash-2", specs[1]).catch(console.error);

        },

        error: function (error_msg) {
            $('.error')
                .text(error_msg)
                .css('visibility', 'visible');
            setTimeout(function () {
                $('.error').css('visibility', 'hidden');
            }, 3000)
        }
    };
}();

// Create the controller
ns.controller = (function (m, v) {
    'use strict';

    let model = m,
        view = v,
        $event_pump = $('body'),
        $person_id = $('#person_id'),
        $name = $('#name'),
        $surname = $('#surname');
    ;

    // Store global datasets
    var people_data,
        projects_data,
        analyses_data,
        samples_data,
        results_data;

    // store global filter variables for each form filter option
    var selected_projects = 'all',
        selected_analyses = 'all',
        date_start = 'all',
        date_end = 'all',
        selected_analysts = 'all',
        selected_departments = 'all',
        available_metadata = [],
        available_metrics = [],
        excluded_samples = 'none';

    // store state information to track metadata filter ids
    var metadata_id_counter = 0,
        applied_metadata_filters = 'none';

    // store sample basket information
    var samples_in_basket = 'none';

    // visualisation global state variables
    var visualise_view_clicked = false,
        arrPlotSpecs = [];

    // Get the data from the model after the controller is done initializing
    setTimeout(function () {
        // model.read_people();
        // model.read_projects();
        // model.read_analyses();
        // model.read_samples();
        model.read_results();

    }, 100)

    // Validate input
    function validate(name, surname) {
        return name !== "" && surname !== "";
    }

    // Build the REST command to fetch remaining samples after filtering
    function build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters) {

        // filter on project Ids
        var query_base = '?';
        query_base += 'project_ids=';

        if (selected_projects == 'all') {
            query_base += 'all';
        } else {
            for (let i = 0, l = selected_projects.length; i < l; i++) {
                query_base += selected_projects[i] + ',';
            }
        }
        ;

        // filter on analysis Ids
        query_base += '&analysis_ids=';

        if (selected_analyses == 'all') {
            query_base += 'all';
        } else {
            for (let i = 0, l = selected_analyses.length; i < l; i++) {
                query_base += selected_analyses[i] + ',';
            }
        }
        ;

        // filter by date
        query_base += '&date_start=' + date_start + '&date_end=' + date_end;

        // filter by analyst name
        query_base += '&selected_analysts=';

        if (selected_analysts == 'all') {
            query_base += 'all';
        } else {
            for (let i = 0, l = selected_analysts.length; i < l; i++) {
                query_base += selected_analysts[i] + ',';
            }
        }
        ;

        // filter by department name
        query_base += '&selected_departments=';

        if (selected_departments == 'all') {
            query_base += 'all';
        } else {
            for (let i = 0, l = selected_departments.length; i < l; i++) {
                query_base += selected_departments[i] + ',';
            }
        }
        ;

        // filter out individual sample Ids
        query_base += '&excl_sample_ids=';

        if (excluded_samples == 'none') {
            query_base += 'none';
        } else {
            for (let i = 0, l = excluded_samples.length; i < l; i++) {
                query_base += excluded_samples[i].id + ',';
            }
        }
        ;

        // filter out samples by applied metadata filters
        query_base += '&metadata_filters=';

        if (applied_metadata_filters == 'none') {
            query_base += 'none';
        } else {
            for (let i = 0, l = applied_metadata_filters.length; i < l; i++) {
                let filter = '{{' + applied_metadata_filters[i].meta_category_name + '}}' +
                    '{{' + applied_metadata_filters[i].meta_operation + '}}' +
                    '{{' + applied_metadata_filters[i].meta_criteria + '}}';
                query_base += filter + ',';
            }
        }

        query_base = query_base.replace(/,&/g, '&');  // remove trailing commas after final list items
        var query = query_base.replace(/,$/g, '');  // remove trailing comma at end of API string

        // return the query
        return query

    }

    function build_metadata_query(samples_in_basket) {

        // filter on sample Ids
        var query_base = '?';
        query_base += 'sample_ids=';

        if (samples_in_basket == 'all') {
            query_base += 'all';
        } else if (samples_in_basket == 'none') {
            query_base += 'none';
        } else {
            for (let i = 0, l = samples_in_basket.length; i < l; i++) {
                query_base += samples_in_basket[i].id + ',';
            }
        }
        ;

        var query = query_base.replace(/,$/g, '');  // remove trailing commas after final list items

        // return the query
        return query
    }

    // Determine if sample ids in basket is different after a selection
    function determineIfBasketChanged(data) {
        let oldIds = [];
        let newIds = [];

        if (data) {
            for (let i = 0; i < data.length; i++) {
                newIds.push(data[i].id);
            }
        }
        ;

        if (samples_in_basket != 'none') {
            for (let i = 0; i < samples_in_basket.length; i++) {
                oldIds.push(samples_in_basket[i].id);
            }
        }
        ;

        if (oldIds === newIds) return false;
        if (oldIds == null || newIds == null) return true;
        if (oldIds.length != newIds.length) return true;

        // If you don't care about the order of the elements inside
        // the array, you should sort both arrays here.
        // Please note that calling sort on an array will modify that array.
        // you might want to clone your array first.

        for (var i = 0; i < oldIds.length; ++i) {
            if (oldIds[i] !== newIds[i]) return true;
        }
        return false;
    };

    // initialise the calendar ui
    $('#rangestart').calendar({
        type: 'month',
        disableMinute: true,
        endCalendar: $('#rangeend'),
        formatter: {
            date: function (date, settings) {
                if (!date) return 'all';
                var day = String(date.getDate());
                if (day.length == 1) {
                    day = '0' + day;
                }
                ;
                var month = String(date.getMonth() + 1);
                if (month.length == 1) {
                    month = '0' + month;
                }
                ;
                var year = date.getFullYear();
                return year + '-' + month + '-' + day;
            }
        },
        onChange: function (date, text, mode) {
            update_date_start(text);
        }
    }).calendar('set startDate', '2015-02-02');

    $('#rangeend').calendar({
        type: 'month',
        disableMinute: true,
        startCalendar: $('#rangestart'),
        formatter: {
            date: function (date, settings) {
                if (!date) return 'all';

                var day = String(date.getDate());
                if (day.length == 1) {
                    day = '0' + day;
                }
                ;

                var month = String(date.getMonth() + 1);
                if (month.length == 1) {
                    month = '0' + month;
                }
                ;

                if (['09', '04', '06', '11'].indexOf(month) > -1) {
                    day = '30';
                } else if (['02'].indexOf(month) > -1) {
                    day = '28';  // Note that I have not yet adjusted for leap years
                } else {
                    day = '31';
                }
                ;

                var year = date.getFullYear();
                return year + '-' + month + '-' + day;
            }
        },
        onChange: function (date, text, mode) {
            update_date_end(text);
        }
    });

    function update_metadata_filters() {
        let query_base = '';
        let metadata_groups = $('.metadata-group');
        let current_meta_filters = [];

        for (let i = 0, l = metadata_groups.length; i < l; i++) {

            // fetch the user supplied metadata parameters
            // ToDo: replace the ugly jQuery here
            let meta_category_name = $($($('.metadata-group')[i]).find('.meta-value')[0]).find('select').val();
            let meta_operation = $($($('.metadata-group')[i]).find('.meta-value')[1]).find('select').val();
            let meta_criteria = $($($('.metadata-group')[i]).find('.meta-value')[2]).val();

            // build the filter
            // ToDO: sanitize the api query that can be passed in
            if (meta_category_name != '' && meta_operation != '' && meta_criteria != '') {
                let filter = {
                    'meta_category_name': meta_category_name,
                    'meta_operation': meta_operation,
                    'meta_criteria': meta_criteria,
                }
                current_meta_filters.push(filter);
            }

        }

        // set global variable so that api queries can be performed holistically from other change events
        if (current_meta_filters.length == 0) {
            current_meta_filters = 'none';
        }

        return current_meta_filters;
    }

    function remove_selected_samples(selected_rows) {

        if (selected_rows.length != 0) {
            if (excluded_samples == 'none') {
                excluded_samples = [];
            }

            for (let i = 0, j = selected_rows.length; i < j; i++) {
                let exclude = {'id': selected_rows[i].id, 'sample': selected_rows[i].sample};
                excluded_samples.push(exclude);
            }
            ;
        }

        if (excluded_samples != 'none' && excluded_samples.length > 0) {
            $('#removed-samples-div').hide();
            $('#removed-samples-div').siblings().remove();

            // insert labels for each excluded sample in the removed samples div
            for (let i = 0, j = excluded_samples.length; i < j; i++) {

                let label_excluded = `<div data-sample-id-tag=${excluded_samples[i].id}
                    class="ui floating compact red message remove-individual-sample-message" style='margin:5px;'>
                        <i class="close icon"></i>
                        <strong>id: </strong>${excluded_samples[i].id}<br>
                        <strong>sample: </strong>${excluded_samples[i].sample}
                    </div>`;
                $('#removed-samples-div').after(label_excluded);
            }
            ;
        } else {
            $('#removed-samples-div').show();
        }

        // build the api query using global variables and return new sample set
        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query

    }

    // Create our event handlers
    $(document).ready(function () {
        model.fetch_dashboard_plot_spec();
    });

    $(document).on('change', '#project-selection-dropdown', function (e) {

        selected_projects = $('#project-selection-dropdown').val();
        if (selected_projects == '') {
            selected_projects = 'all';
        }

        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query
    });

    $(document).on('change', '.metadata-group', function (e) {
        // update global metadata filters
        applied_metadata_filters = update_metadata_filters();

        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query
    });

    $(document).on('click', '.message.metadata-group-message .close', function () {
        // remove filter node from the DOM
        $(this).closest('.message').transition('fade').remove();

        // update global metadata filters
        applied_metadata_filters = update_metadata_filters();

        // update sample basket
        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query
    });


    $(document).on('click', '.message.remove-individual-sample-message .close', function () {
        // determine id of sample to un-exclude
        let temp_samples = [];
        let unexclude_sample = $(this).closest('[data-sample-id-tag]').attr('data-sample-id-tag');

        // update global variable for excluded samples
        for (let i = 0, j = excluded_samples.length; i < j; i++) {
            if (excluded_samples[i].id != unexclude_sample) {
                temp_samples.push(excluded_samples[i])
            }
        }
        excluded_samples = temp_samples;

        // remove filter node from the DOM
        $(this).closest('.message').transition('fade').remove();
        if (excluded_samples.length == 0) {
            $('#removed-samples-div').show();
            excluded_samples = 'none';
        }

        // update sample basket
        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query
    });

    $(document).on('click', '.message.graph-message .close', function () {
        // fetch the value of the graph to remove
        let plot_to_remove = $(this).parent().find('div[data-plot-id]').attr('data-plot-id');

        // remove graph from the DOM
        $(this).closest('.message').transition('fade').remove();

        // update global plot list
        arrPlotSpecs[plot_to_remove] = {};
        //arrPlotSpecs.splice(plot_to_remove, 1);

        // if there are no plots left then display the placeholder
        let plotsEmpty = true;
        for (let i = 0, j = arrPlotSpecs.length; i < j; i++) {
            if (Object.keys(arrPlotSpecs[i]).length != 0) {
                plotsEmpty = false;
            }
        }
        if (plotsEmpty) {
            view.plot_graphs([]);
        }

    });


    $(document).on('change', '#analysis-selection-dropdown', function (e) {

        selected_analyses = $('#analysis-selection-dropdown').val();
        if (selected_analyses == '') {
            selected_analyses = 'all';
        }

        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query
    });

    $(document).on('change', '#department-selection-dropdown', function (e) {

        selected_departments = $('#department-selection-dropdown').val();
        if (selected_departments == '') {
            selected_departments = 'all';
        }

        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query
    });

    $(document).on('change', '#analyst-selection-dropdown', function (e) {

        selected_analysts = $('#analyst-selection-dropdown').val();
        if (selected_analysts == '') {
            selected_analysts = 'all';
        }

        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query
    });


    $(document).on('click', "#add-new-metadata-filter-button", function (e) {

        // helps to prevent empty metadata filters from being created
        // ToDo: replace with more robust method where creation is only possible
        //     : if the samples in basket actually contain metadata
        if (samples_in_basket != 'none') {
            // increment meta id field counter
            metadata_id_counter += 1;

            // build API query for available metadata tags
            let query = build_metadata_query(samples_in_basket);

            // fetch metadata and store globally
            model.fetch_metadata(query);

            // build template for metadata fields
            view.add_new_metadata_filter(metadata_id_counter);

            // populate metadata template with results from API call above
            // ToDo: replace the following with a callback/promise (figure this out still)
            setTimeout(function () {
                view.populate_metadata_filter(metadata_id_counter, available_metadata);
            }, 100)

        } else {
            // display warning message for 6 seconds, after which it fades out
            // Todo: upon clicking the close button, the message still takes up space for 6 seconds
            $('#metadata-filter-error-msg').addClass('active');

            setTimeout(function () {
                $('#metadata-filter-error-msg').addClass('out').removeClass('active');
                setTimeout(function () {
                    $('#metadata-filter-error-msg').removeClass('out');
                }, 500); //Same time as animation

            }, 6000)

        }
        ;
    });

    $(document).on('click', '#link-to-manual-modification', function (e) {
        model.read_people();
        model.read_projects();
        model.read_analyses();
        model.read_samples();
    });

    $(document).on('click', '#select-samples-menu-selection', function (e) {
        model.read_people();
        model.read_projects();
        model.read_analyses();
        model.read_samples();
    });

    $(document).on('click', '#visualise-menu-selection', function (e) {

        if (samples_in_basket.length >= 1 && samples_in_basket != 'none') {
            $('#plot-fields-warning-msg').addClass('hidden').removeClass('active');
            $('#add-new-graph-button').removeClass('disabled');

            // build API query for available metadata tags
            let query = build_metadata_query(samples_in_basket);

            // fetch metadata and store globally
            model.fetch_metadata(query);

            // update global variables for metrics and group-by
            model.fetch_available_metrics(query);

            // populate first plotting parameter field group
            setTimeout(function () {
                view.add_new_plotting_fields(available_metrics, available_metadata);
            }, 350); // NB: needed due to delayed response from server
                     // ToDo: need to figure out a way to use callbacks/promises rather

            // show graph placeholder if no graphs in the graph array
            if (arrPlotSpecs.length == 0) {
                view.plot_graphs([]);
            }

        } else {
            // change class of button and fields to disable\
            $('#add-new-graph-button').addClass('disabled');

            // show message warning of no samples present
            $('#plot-fields-warning-msg').addClass('active');

            // add placeholder for plotting fields
            view.add_new_plotting_fields([], []);
            $('#plotting-field-group').addClass('disabled');

            // show graph placeholder if no graphs in the graph array
            if (arrPlotSpecs.length == 0) {
                view.plot_graphs([]);
            }

        }


    })

    $(document).on('click', '#add-new-graph-button', function (e) {
        // fetch the selected values from the form fields
        let selected_metric = $('#plotting-field-0').val();
        let selected_plot_type = $('#plotting-field-1').val();
        let selected_category = $('#plotting-field-2').val();

        // fetch graph altair spec if fields not empty
        if (selected_metric && selected_plot_type && selected_category) {
            $('#plot-fields-error-msg').removeClass('active');
            let query = '?sample_ids=';

            if (samples_in_basket.length >= 1 && samples_in_basket != 'none') {
                for (let i = 0, l = samples_in_basket.length; i < l - 1; i++) {
                    query += samples_in_basket[i].id + ',';
                }
                ;
                query += samples_in_basket[samples_in_basket.length - 1].id;  // no trailing ',' for last element
            } else {
                query += 'none';
            }

            query += '&metric_name=' + selected_metric;
            query += '&plot_type=' + selected_plot_type;
            query += '&grouping_category=' + selected_category;

            model.fetch_vega_plot_spec(query);


            // insert new graph at the top by displaying each graph in the array
            setTimeout(function () {
                view.plot_graphs(arrPlotSpecs);
            }, 300);

        } else {
            // show warning message saying fields have not been completed
            $('#plot-fields-error-msg').addClass('active');

            setTimeout(function () {
                $('#plot-fields-error-msg').addClass('out').removeClass('active');
                setTimeout(function () {
                    $('#plot-fields-error-msg').removeClass('out');
                }, 500); //Same time as animation
            }, 5000)
        }


    });

    // date change functions called by the calendar's internal event handler
    function update_date_start(date) {
        date_start = date;
        if (date_start == '') {
            date_start = 'all';
        }
        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query
    };

    function update_date_end(date) {
        date_end = date;
        if (date_end == '') {
            date_end = 'all';
        }
        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples, applied_metadata_filters);
        model.read_combined_sample_data(query);  // run the query
    };

    $('#create').click(function (e) {
        let name = $name.val(),
            surname = $surname.val();

        e.preventDefault();

        if (validate(name, surname)) {
            model.create({
                'name': name,
                'surname': surname,
            })
        } else {
            alert('Problem with first or last name input');
        }
    });

    $('#update').click(function (e) {
        let person_id = $person_id.val(),
            name = $name.val(),
            surname = $surname.val();

        e.preventDefault();

        if (validate(name, surname)) {
            model.update({
                person_id: person_id,
                name: name,
                surname: surname,
            })
        } else {
            alert('Problem with first or last name input');
        }
        e.preventDefault();
    });

    $('#delete').click(function (e) {
        let person_id = $person_id.val();

        e.preventDefault();

        if (validate('placeholder', surname)) {
            model.delete(person_id)
        } else {
            alert('Problem with first or last name input');
        }
        e.preventDefault();
    });

    $('#reset').click(function () {
        view.reset();
    })

    $('#people-table').on('dblclick', 'tbody > tr', function (e) {
        //note that this functionality has been temporarily removed as it will be replaced
        // by native dataTables functionality in the near future.
        // For now, the code has been changed to "work" with the new dataTables formatted
        // table, but it incorrectly references the old cell ids.
        let $target = $(e.target),
            person_id,
            name,
            surname;

        person_id = $target
            .parent()
            .attr('data-person-id');

        name = $target
            .parent()
            .find('td.name')
            .text();

        surname = $target
            .parent()
            .find('td.surname')
            .text();

        view.update_editor({
            person_id: person_id,
            name: name,
            surname: surname,
        });
    });

    // Handle the model events
    $event_pump.on('model_read_success_people', function (e, data) {
        view.build_table_people(data);
        people_data = data;
        view.reset();
    });
    $event_pump.on('model_read_success_projects', function (e, data) {
        projects_data = data;
        view.build_table_projects(projects_data);
        view.populate_projects_search(projects_data);
        view.reset();
    });
    $event_pump.on('model_read_success_analyses', function (e, data) {
        analyses_data = data;
        view.build_table_analyses(analyses_data); // move out to when add custom analysis is clicked
        view.populate_analyses_search(analyses_data);
        view.populate_departments_search(analyses_data);  // Note: data is part of the analyses table
        view.populate_analysts_search(analyses_data);
        view.reset();
    });
    $event_pump.on('model_read_success_samples', function (e, data) {
        view.build_table_samples(data);
        samples_data = data;
        view.reset();
    });
    $event_pump.on('model_read_success_combined_sample_data', function (e, data) {
        let isChanged = determineIfBasketChanged(data),
            samples_count;

        if (data.length > 0) {
            samples_in_basket = data;  // update the global basket variable
            samples_count = samples_in_basket.length;
        } else {
            samples_in_basket = 'none';
            samples_count = 0;
        }

        //populate samples table in DOM
        // should move the loader out into the appropriate view function
        $('#sample-selection-results-loader').addClass('hidden').removeClass('active');
        view.build_search_results_table(samples_in_basket, remove_selected_samples);
        view.update_basket(samples_count, isChanged);
        view.reset();
    });
    $event_pump.on('model_read_success_metadata', function (e, data) {

        if (data.length > 0) {
            available_metadata = data;  // update the global metadata variable
        } else {
            available_metadata = [];
        }

        view.reset();
    });
    $event_pump.on('model_read_success_metrics', function (e, data) {

        if (data.length > 0) {
            available_metrics = data;  // update the global metrics variable
        } else {
            available_metrics = [];
        }

        view.reset();
    });
    $event_pump.on('model_read_success_fetch_vega_plot_spec', function (e, data) {
        let vega_spec = data;
        arrPlotSpecs.unshift(vega_spec);
        view.reset();
    });
    $event_pump.on('model_read_success_fetch_dashboard_plot_spec', function (e, data) {
        let vega_specs = data;
        view.create_dashboard_plot(vega_specs);
        view.reset();
    });

    $event_pump.on('model_read_success_results', function (e, data) {
        view.build_table_results(data);
        results_data = data;
        view.reset();
    });

    $event_pump.on('model_create_success', function (e, data) {
        model.read_people();
    });

    $event_pump.on('model_update_success', function (e, data) {
        model.read_people();
    });

    $event_pump.on('model_delete_success', function (e, data) {
        model.read_people();
    });

    $event_pump.on('model_error', function (e, xhr, textStatus, errorThrown) {
        let error_msg = textStatus + ': ' + errorThrown + ' - ' + xhr.responseJSON.detail;
        view.error(error_msg);
        console.log(error_msg);
    });

}(ns.model, ns.view));


