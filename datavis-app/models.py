from datetime import datetime

from config import db, ma
from sqlalchemy.orm import backref


class Person(db.Model):
    """
    Lookup table containing personnel information.

    - Fields::

        id [INT, PK]
        name [STR(50), UNIQUE]
        surname [STR(50)]
        timestamp [DATETIME(utc), DEFAULT(current date time)]

    """

    __tablename__ = "person"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    surname = db.Column(db.String(50))
    # ToDo: remove timestamp column or find a use for it
    timestamp = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow
    )

    def __repr__(self):
        """representation of the Person model"""
        return (
            f"<Person(id='{self.person_id}', name='{self.name}', path='{self.surname}'>"
        )


class PersonSchema(ma.ModelSchema):
    class Meta:
        model = Person
        sqla_session = db.session


class Project(db.Model):
    """
    Lookup table containing project information

    - Fields::

        id [INT, PK]
        name [STR(50), UNIQUE]
        path [STR(300), UNIQUE]

    """

    __tablename__ = "project"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    path = db.Column(db.String(300), unique=True)

    def __repr__(self):
        """representation of the Project model"""
        return f"<Project(id='{self.id}', name='{self.name}', path='{self.path}'>"


class ProjectSchema(ma.ModelSchema):
    class Meta:
        model = Project
        sqla_session = db.session


class Analysis(db.Model):
    """
    Table containing analysis information

    - Fields::

        id [INT, PK]
        name [STR(50), UNIQUE, REQUIRED]
        description [STR(300)]
        date [DATE(utc), DEFAULT(current date)]
        department [STR(20), REQUIRED]
        analyst_id [INT, REQUIRED, FK(person.id)]
        project_id [INT, REQUIRED, FK(project.id)]

    """

    __tablename__ = "analysis"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    description = db.Column(db.String(300))
    date = db.Column(db.Date, default=db.func.now())
    department = db.Column(db.String(20), nullable=False)
    # ToDo: path to results
    analyst_id = db.Column(db.Integer, db.ForeignKey("person.id"), nullable=False)
    analyst = db.relationship("Person", backref="analysis")
    project_id = db.Column(db.Integer, db.ForeignKey("project.id"), nullable=False)
    project = db.relationship("Project", backref="analysis")

    def __repr__(self):
        """representation of the Analysis model"""
        return f"<Analysis(id='{self.id}', name='{self.name}', description='{self.description}', date='{self.date}', department='{self.department}', analyst='{self.analyst}', project='{self.project_id}'>"


class AnalysisSchema(ma.ModelSchema):
    """
    Check for the *existence* of the field during validation when calling .load()

    Note: the validation performed is the same as the following:
    name = fields.String(required=True, validate=[validate.Length(max=50)])
    description = fields.String(allow_none=True, validate=[validate.Length(max=300)])
    date = fields.Date(allow_none=True)  # field must be present but value can be None
    department = fields.String(required=True, validate=[validate.Length(max=20)])
    analyst = fields.String(required=True)
    project_id = fields.String(required=True)
    """

    class Meta:
        model = Analysis
        fields = (
            "id",
            "name",
            "description",
            "date",
            "department",
            "analyst_id",
            "project_id",  # note: consider whether this should rather be "project"
        )
        sqla_session = db.session


class Sample(db.Model):
    """
    Semi-structured table containing sample metadata information

    - Fields::

        id [INT, PK]
        name [STR(50), REQUIRED]
        sample_metadata [STR]
        analysis_id [INT, REQUIRED, FK(analysis.id)]

    """

    __tablename__ = "sample"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    sample_metadata = db.Column(db.String())
    analysis_id = db.Column(db.Integer, db.ForeignKey("analysis.id"), nullable=False)
    analysis = db.relationship("Analysis", backref=backref("sample", cascade="delete"))

    def __repr__(self):
        """representation of the Sample model"""
        return f"<Sample(id='{self.id}', name='{self.name}', sample_metadata='{self.sample_metadata}', analysis_id='{self.analysis_id}'>"


class SampleSchema(ma.ModelSchema):
    """
    Check for the *existence* of the field during validation when calling .load()
    """

    class Meta:
        model = Sample
        sqla_session = db.session
        fields = ("id", "name", "sample_metadata", "department", "analysis_id")
        sqla_session = db.session


class Result(db.Model):
    """
    Semi-structured table containing sample result information

    - Fields::

        id [INT, PK]
        metrics = [STR]
        sample_id [INT, REQUIRED, FK(sample.id)]

    """

    __tablename__ = "result"
    id = db.Column(db.Integer, primary_key=True)
    metrics = db.Column(db.String())
    sample_id = db.Column(db.Integer, db.ForeignKey("sample.id"), nullable=False)
    sample = db.relationship("Sample", backref=backref("result", cascade="delete"))

    def __repr__(self):
        """representation of the Result model"""
        return f"<Result(id='{self.id}', metrics='{self.metrics}', sample_id='{self.sample_id}'>"


class ResultSchema(ma.ModelSchema):
    """
    Check for the *existence* of the field during validation when calling .load()
    """

    class Meta:
        model = Result
        sqla_session = db.session
        fields = ("id", "metrics", "sample_id")
        sqla_session = db.session
