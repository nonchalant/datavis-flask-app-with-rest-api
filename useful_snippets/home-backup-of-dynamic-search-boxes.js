/*
 * JavaScript file for the application to demonstrate
 * using the API
 */

// Create the namespace instance
let ns = {};

// Create the model instance
ns.model = (function() {
    'use strict';

    let $event_pump = $('body'),
        samples_in_basket;


    // Return the API
    return {
        'read_people': function() {
            let ajax_options = {
                type: 'GET',
                url: 'api/people',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success_people', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'read_projects': function() {
            let ajax_options = {
                type: 'GET',
                url: 'api/projects',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success_projects', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'read_analyses': function() {
            let ajax_options = {
                type: 'GET',
                url: 'api/analyses',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success_analyses', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'read_samples': function() {
            let ajax_options = {
                type: 'GET',
                url: 'api/samples',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success_samples', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'read_combined_sample_data': function(api_filter_str) {
            let ajax_options = {
                type: 'GET',
                url: 'api/samples/combine_and_filter'+api_filter_str,
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success_combined_sample_data', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'read_results': function() {
            let ajax_options = {
                type: 'GET',
                url: 'api/results',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success_results', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        create: function(person) {
            let ajax_options = {
                type: 'POST',
                url: 'api/people',
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(person)
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_create_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        update: function(person) {
            let ajax_options = {
                type: 'PUT',
                url: `api/people/${person.person_id}`,
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(person)
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_update_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'delete': function(person_id) {
            let ajax_options = {
                type: 'DELETE',
                url: `api/people/${person_id}`,
                accepts: 'application/json',
                contentType: 'plain/text'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_delete_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        }
    };
}());

// Create the view instance
ns.view = (function() {
    'use strict';

    let $person_id = $('#person_id'),
        $name = $('#name'),
        $surname = $('#surname');

    // return the API
    return {
        reset: function() {
            $person_id.val('');
            $surname.val('');
            $name.val('').focus();
        },
        update_editor: function(person) {
            $person_id.val(person.person_id);
            $surname.val(person.surname);
            $name.val(person.name).focus();
        },
        build_table_people: function(people) {
            let columns = [];

            for (let i=0, l=Object.keys(people[0]).length; i < l; i++){
                let col = Object.keys(people[0])[i];  // grab the key name; e.g. "Sample"
                columns.push({ "title": col, "data": col });  // this format is needed for dynamic columns
            };

            $("#people-table").DataTable({
                "data": people,
                "columns": columns
            });

        },

        build_table_projects: function(projects) {
            let columns = [];

            for (let i=0, l=Object.keys(projects[0]).length; i < l; i++){
                let col = Object.keys(projects[0])[i];  // grab the key name; e.g. "Sample"
                columns.push({ "title": col, "data": col });  // this format is needed for dynamic columns
            };

            $("#projects-table").DataTable({
                "data": projects,
                "columns": columns
            });

        },
        populate_projects_search: function(selected_projects, samples_in_basket) {
            console.log('running pop prokjects search');
            console.log('   selected projects: ', selected_projects);
            console.log('   samples_in_basket: ', samples_in_basket);
            // clear the dropdown
            $('#project-selection-dropdown').empty();
            $( "select#project-selection-dropdown ~ a" ).remove()  // removes all a tags adjacent to this select tag in the DOM
            var options = '';
            var selected_tags = '';
            var uniqueIds = [];

            // loop through all projects and add to options list in dropdown
            for (let i=0, l=samples_in_basket.length; i < l; i++) {
                if (uniqueIds.includes(String(samples_in_basket[i].project_id))){
                    console.log('not unique Id - skipping...');
                }
                else{
                    uniqueIds.push(String(samples_in_basket[i].project_id));

                    options += `<option value="${samples_in_basket[i].project_id}">${samples_in_basket[i].project_name}</option>`;

                    // re-populate the 'already selected' list in the DOM
                    if (selected_projects.includes(String(samples_in_basket[i].project_id))){
                        selected_tags += `<a class="ui label transition visible"
                        data-value="${samples_in_basket[i].project_id}"
                        style="display: inline-block !important;">
                        ${samples_in_basket[i].project_name}
                        <i class="delete icon"></i></a>`
                    }
                }
            }

            console.log('uniqueIds: ', uniqueIds, 'options: ', options, 'selected_tags: ', selected_tags);

            $('#project-selection-dropdown').append(options);
            $('select#project-selection-dropdown ~ i').after(selected_tags);
            $('#project-selection-dropdown').val(uniqueIds);  // finally, set the "selected options" in the form


        },
        populate_analyses_search: function(selected_analyses, samples_in_basket) {
            console.log('running pop analyses search');
            console.log('selected analyses: ', selected_analyses);
            // clear the dropdown
            $('#analysis-selection-dropdown').empty();  // deletes the values selected in the field
            $( "select#analysis-selection-dropdown ~ a" ).remove()  // removes all a tags adjacent to this select tag in the DOM
            var options = '';
            var selected_tags = '';
            var uniqueIds = [];

            // loop through all analysis names and add to options list in dropdown
            for (let i=0, l=samples_in_basket.length; i < l; i++) {
                if (uniqueIds.includes(String(samples_in_basket[i].project_id))){
                    console.log('not unique Id - skipping...');
                }
                else{
                    uniqueIds.push(String(samples_in_basket[i].project_id));

                    options += `<option value="${samples_in_basket[i].analysis_id}">${samples_in_basket[i].analysis_name}</option>`;

                    // re-populate the 'already selected' list in the DOM
                    if (selected_analyses.includes(String(samples_in_basket[i].analysis_id))){
                        selected_tags += `<a class="ui label transition visible"
                        data-value="${samples_in_basket[i].analysis_id}"
                        style="display: inline-block !important;">
                        ${samples_in_basket[i].analysis_name}
                        <i class="delete icon"></i></a>`
                    }
                }
            }

            console.log('uniqueIds: ', uniqueIds, 'options: ', options, 'selected_tags: ', selected_tags);

            $('#analysis-selection-dropdown').append(options);
            $('select#analysis-selection-dropdown ~ i').after(selected_tags);
            $('#analysis-selection-dropdown').val(uniqueIds);  // finally, set the "selected options" in the form

        },
//        populate_analyses_search: function(selected_projects, analyses_data) {
//            // clear the dropdown options and store already selected
//            var current_selection = $('#analysis-selection-dropdown').val();
//            var selected_tags = ''
//            var options = ''
//            $('#analysis-selection-dropdown').empty();  // clears dropdown options and selected values
//            $( "select#analysis-selection-dropdown ~ a" ).remove()  // removes all a tags adjacent to this select tag
//
//            // populate the dropdown options with those still valid under new filter
//            if (selected_projects) {
//                for (let i=0, l=analyses_data.length; i < l; i++) {
//                    for (let j=0, k=selected_projects.length; j < k; j++){
//                        if(analyses_data[i].project == selected_projects[j] || selected_projects == 'all'){
//                            options += `<option value="${analyses_data[i].analysis_id}">${analyses_data[i].name}</option>`;
//
//                            // re-populate the 'already selected' list
//                            if (current_selection.includes(String(analyses_data[i].analysis_id))){
//                                selected_tags += `<a class="ui label transition visible"
//                                data-value="${analyses_data[i].analysis_id}"
//                                style="display: inline-block !important;">
//                                ${analyses_data[i].name}
//                                <i class="delete icon"></i></a>`
//                            }
//
//                        };
//
//                    };
//
//                };
//
//                $('#analysis-selection-dropdown').append(options);
//                $( 'select#analysis-selection-dropdown ~ i ').after(selected_tags);
//            }
//
//        },

        build_table_analyses: function(analyses) {
            let columns = [];

            for (let i=0, l=Object.keys(analyses[0]).length; i < l; i++){
                let col = Object.keys(analyses[0])[i];  // grab the key name; e.g. "Sample"
                columns.push({ "title": col, "data": col });  // this format is needed for dynamic columns
            };

            $("#analyses-table").DataTable({
                "data": analyses,
                "columns": columns
            });

        },
        build_table_samples: function(samples) {
            let columns = [];

            for (let i=0, l=Object.keys(samples[0]).length; i < l; i++){
                let col = Object.keys(samples[0])[i];  // grab the key name; e.g. "Sample"
                columns.push({ "title": col, "data": col });  // this format is needed for dynamic columns
            };

            $("#samples-table").DataTable({
                "data": samples,
                "columns": columns
            });

        },
        build_table_results: function(results) {
            let columns = [];

            for (let i=0, l=Object.keys(results[0]).length; i < l; i++){
                let col = Object.keys(results[0])[i];  // grab the key name; e.g. "Sample"
                columns.push({ "title": col, "data": col });  // this format is needed for dynamic columns
            };

            $("#results-table").DataTable({
                "data": results,
                "columns": columns
            });

        },
        error: function(error_msg) {
            $('.error')
                .text(error_msg)
                .css('visibility', 'visible');
            setTimeout(function() {
                $('.error').css('visibility', 'hidden');
            }, 3000)
        }
    };
}());

// Create the controller
ns.controller = (function(m, v) {
    'use strict';

    let model = m,
        view = v,
        $event_pump = $('body'),
        $person_id = $('#person_id'),
        $name = $('#name'),
        $surname = $('#surname');
;

    // Store global datasets
    var people_data,
        projects_data,
        analyses_data,
        samples_data,
        results_data;

    // store global filter variables for each form filter option
    var selected_projects = 'all',
        selected_analyses = 'all',
        date_start = 'all',
        date_end = 'all',
        selected_analysts = 'all',
        selected_departments = 'all',
        excluded_samples = 'none';

    // store sample basket information
    var samples_in_basket;

    // Get the data from the model after the controller is done initializing
    setTimeout(function() {
        model.read_people();
        model.read_projects();
        model.read_analyses();
        model.read_samples();
        model.read_results();

    }, 100)

    // generate a combined dataset of all sample information for filtering
//    function combine_all(projects_data, analyses_data, samples_data){
//        console.log('projects_data and ana data:', projects_data, analyses_data)
//        function join(lookupTable, mainTable, lookupKey, mainKey){
//            var l = lookupTable.length,
//                m = mainTable.length,
//                lookupIndex = [],
//                output = [];
//            for (var i = 0; i < l; i++) { // loop through l items
//                var row = lookupTable[i];
//                lookupIndex[row[lookupKey]] = row; // create an index for lookup table
//            }
//            for (var j = 0; j < m; j++) { // loop through m items
//                var y = mainTable[j];
//                var x = lookupIndex[y[mainKey]]; // get corresponding row from lookupTable
//                output.push([x,y]); // select only the columns you need
//            }
//            return output;
//        };
//
//        var projects_join_analyses = join(projects_data, analyses_data, 'project_id', 'project');
//        console.log('joined:   ', projects_join_analyses);
//
//    }


    // Validate input
    function validate(name, surname) {
        return name !== "" && surname !== "";
    }

    // Build and run the REST command to fetch remaining samples after filtering
    function build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples){
        console.log('running generate sample basket fnctn');
        console.log('selected projects, analyses, date s, date e, selected analysts, selected depart', selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments);

        // generate the query
        var query_base = '?';
            query_base += 'project_ids=';

        if (selected_projects == 'all'){
            query_base += 'all';
        }
        else{
            for(let i=0, l=selected_projects.length; i < l; i++){
                 query_base += selected_projects[i] + ',';
            }
        };

        query_base += '&analysis_ids=';

        if (selected_analyses == 'all'){
            query_base += 'all';
        }
        else{
            for(let i=0, l=selected_analyses.length; i < l; i++){
                 query_base += selected_analyses[i] + ',';
            }
        };

        query_base += '&date_start='+date_start+'&date_end='+date_end;

        query_base += '&selected_analysts=';

        if (selected_analysts == 'all'){
            query_base += 'all';
        }
        else{
            for(let i=0, l=selected_analysts.length; i < l; i++){
                 query_base += selected_analysts[i] + ',';
            }
        };

        query_base += '&selected_departments=';

        if (selected_departments == 'all'){
            query_base += 'all';
        }
        else{
            for(let i=0, l=selected_departments.length; i < l; i++){
                 query_base += selected_departments[i] + ',';
            }
        };

        query_base += '&excl_sample_ids=';

        if (excluded_samples == 'none'){
            query_base += 'none';
        }
        else{
            for(let i=0, l=excluded_samples.length; i < l; i++){
                 query_base += excluded_samples[i] + ',';
            }
        };

        var query = query_base.replace(/,&/g, '&');
        console.log(query)

        // run the query
        //model.read_combined_sample_data(query);
        // return the query
        return query

    }

    // Create our event handlers
    $(document).on('change', '#project-selection-dropdown', function (e) {
        console.log('projec selection dropdown changed');

        selected_projects = $('#project-selection-dropdown').val();
        if (selected_projects == ''){
            selected_projects = 'all';
        }
        console.log('selected_projects: ', selected_projects);

        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples);
        model.read_combined_sample_data(query);  // run the query
    });

    $(document).on('change', '#analysis-selection-dropdown', function (e) {
        console.log('analysis selection dropdown changed');

        selected_analyses = $('#analysis-selection-dropdown').val();
        if (selected_analyses == ''){
            selected_analyses = 'all';
        }
        console.log('selected_analyses: ', selected_analyses);

        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples);
        model.read_combined_sample_data(query);  // run the query
    });

    // fetch initial sample information
    $( document ).on('click', '#select-samples-menu-selection', function(e) {
        console.log('clicked menu');

        var query = build_sample_filter_query(selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments, excluded_samples);
        model.read_combined_sample_data(query);  // run the query
    });

//    $(document).on('change', '#analyses-selection-dropdown', function (e) {
//        var selected_analyses = $('#analyses-selection-dropdown').val();
//
//        e.preventDefault();
//
//        view.populate_analyses_search(selected_projects, analyses_data);
//    });

    $('#create').click(function(e) {
        let name = $name.val(),
            surname = $surname.val();

        e.preventDefault();

        if (validate(name, surname)) {
            model.create({
                'name': name,
                'surname': surname,
            })
        } else {
            alert('Problem with first or last name input');
        }
    });

    $('#update').click(function(e) {
        let person_id = $person_id.val(),
            name = $name.val(),
            surname = $surname.val();

        e.preventDefault();

        if (validate(name, surname)) {
            model.update({
                person_id: person_id,
                name: name,
                surname: surname,
            })
        } else {
            alert('Problem with first or last name input');
        }
        e.preventDefault();
    });

    $('#delete').click(function(e) {
        let person_id = $person_id.val();

        e.preventDefault();

        if (validate('placeholder', surname)) {
            model.delete(person_id)
        } else {
            alert('Problem with first or last name input');
        }
        e.preventDefault();
    });

    $('#reset').click(function() {
        view.reset();
    })

    $('#people-table').on('dblclick', 'tbody > tr', function(e) {
        //note that this functionality has been temporarily removed as it will be replaced
        // by native dataTables functionality in the near future.
        // For now, the code has been changed to "work" with the new dataTables formatted
        // table, but it incorrectly references the old cell ids.
        let $target = $(e.target),
            person_id,
            name,
            surname;

        person_id = $target
            .parent()
            .attr('data-person-id');

        name = $target
            .parent()
            .find('td.name')
            .text();

        surname = $target
            .parent()
            .find('td.surname')
            .text();

        view.update_editor({
            person_id: person_id,
            name: name,
            surname: surname,
        });
    });

    // Handle the model events
    $event_pump.on('model_read_success_people', function(e, data) {
        view.build_table_people(data);
        people_data = data;
        view.reset();
    });
    $event_pump.on('model_read_success_projects', function(e, data) {
        projects_data = data;
        view.build_table_projects(projects_data);
       // view.populate_projects_search('all', projects_data);
        view.reset();
    });
    $event_pump.on('model_read_success_analyses', function(e, data) {
        analyses_data = data;
        view.build_table_analyses(analyses_data);
     //   view.populate_analyses_search('all', analyses_data);
        view.reset();
    });
    $event_pump.on('model_read_success_samples', function(e, data) {
        view.build_table_samples(data);
        samples_data = data;
        view.reset();
    });
    $event_pump.on('model_read_success_combined_sample_data', function(e, data) {
        samples_in_basket = data;
        console.log('model_read_success_combined_sample_data::samples_in_basket: ', samples_in_basket)
        console.log('selected projects, analyses, date s, date e, selected analysts, selected depart', selected_projects, selected_analyses, date_start, date_end, selected_analysts, selected_departments);

        //populate all DOM elements
        view.populate_projects_search(selected_projects, samples_in_basket);
        view.populate_analyses_search(selected_analyses, samples_in_basket);

        view.reset();
    });

    $event_pump.on('model_read_success_results', function(e, data) {
        view.build_table_results(data);
        results_data = data;
        view.reset();
    });

    $event_pump.on('model_create_success', function(e, data) {
        model.read_people();
    });

    $event_pump.on('model_update_success', function(e, data) {
        model.read_people();
    });

    $event_pump.on('model_delete_success', function(e, data) {
        model.read_people();
    });

    $event_pump.on('model_error', function(e, xhr, textStatus, errorThrown) {
        let error_msg = textStatus + ': ' + errorThrown + ' - ' + xhr.responseJSON.detail;
        view.error(error_msg);
        console.log(error_msg);
    });






}(ns.model, ns.view));


