alabaster==0.7.12
altair==2.3.0
appdirs==1.4.3
attrs==18.2.0
Babel==2.6.0
backcall==0.1.0
certifi==2018.10.15
clickclick==1.2.2
connexion==2.0.2
decorator==4.3.0
docutils==0.14
entrypoints==0.2.3
Flask==1.0.2
flask-marshmallow==0.9.0
Flask-SQLAlchemy==2.3.2
imagesize==1.1.0
inflection==0.3.1
itsdangerous==1.1.0
jsonschema==2.6.0
Jinja2==2.10
MarkupSafe==1.1.0
marshmallow==3.0.0rc1
marshmallow-sqlalchemy==0.15.0
numpy==1.15.4
openapi-spec-validator==0.2.4
packaging==19.0
pandas==0.23.4
python-dateutil==2.7.5
pytz==2018.7
PyYAML==3.13
requests==2.20.1
six==1.11.0
snowballstemmer==1.2.1
Sphinx==1.8.4
sphinx-rtd-theme==0.4.3
sphinxcontrib-httpdomain==1.7.0
sphinxcontrib-openapi==0.4.0
sphinxcontrib-redoc==1.5.1
sphinxcontrib-websupport==1.1.0
SQLAlchemy==1.2.14
style==1.1.0
swagger-ui-bundle==0.0.2
toml==0.10.0
toolz==0.9.0
urllib3==1.24.1
vega==1.4.0
Werkzeug==0.14.1
