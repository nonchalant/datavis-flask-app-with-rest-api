Useful Resources
=================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

On this page you will find various resources that may be helpful when using this application.

Glossary
*********

.. glossary::

    DataVis App
      Primarily refers to the user facing component or "front-end" of the data management/visualization suite.

    DataVis API
      The underlying RESTful service upon which the DataVis App was built.

    REST API
      A paradigm defining standards for how HTTP requests should be sent across a server to request/add/update and
      remove data from a database. It is important to note that requests made to a REST API are inherently stateless.

