.. DataVis documentation master file, created by
   sphinx-quickstart on Fri Feb 15 09:56:05 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DataVis App and REST API Documentation
======================================

Welcome to the official documentation portal for the DataVis webApp. These docs have been split up into two separate
components which jointly comprise the data management and visualization portal for the RSS Cape Town Bioinformatics team.
The two components are namely the Flask-based web application (henceforth known as the :term:`DataVis App`) and
accompanying RESTful API server (henceforth known as the :term:`DataVis API`).

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   REST_API/index.rst
   DataVis_App/index.rst
   Resources/index.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
