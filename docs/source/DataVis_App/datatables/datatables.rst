Datatables Module
==================


.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. automodule:: __init__

.. automodule:: datatables
   :members: 

