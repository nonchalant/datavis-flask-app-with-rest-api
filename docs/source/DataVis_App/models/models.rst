Database Models
***************

.. automodule:: __init__

The following models describe the database schema.
ToDo: See also graphical depiction below:

.. autoclass:: models.Person
   :members:


.. autoclass:: models.Project
   :members:


.. autoclass:: models.Analysis
   :members:


.. autoclass:: models.Sample
   :members:


.. autoclass:: models.Result
   :members:



   
   