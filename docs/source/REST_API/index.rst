REST API
=========

.. toctree::
   :maxdepth: 3
   :caption: Contents:
      

What is a rest API?
*******************

some text.

How to make standalone requests
*******************************

some text.

some example.

How to use the DataVis REST API
********************************

There are two ways one can choose to navigate the API reference documentation.

The suggested approach is an interactive version of the OpenAPI docs, which makes
for easy navigation for those unfamiliar with using REST APIs (see `Interactive OpenAPI docs` below).

The second approach contains exactly the same information as with the first approach, but is embedded
in this page in the same style and layout as the rest of the documentation described on this site.
This version is probably more suitable for those who know what they are looking for and/or are familiar
with the format of this documentation site.

OpenAPI docs (interactive):
-----------------------------

The Interactive OpenAPI docs can be accessed by following `this resource <api.html>`_.


OpenAPI docs (static):
-----------------------

.. openapi:: ../../../datavis-app/swagger_openapi_v3.0.0.yml