# Datavis flask app with REST api

Proof of concept single page web application using Flask + connexion.
Documentation available at: https://nonchalant.gitlab.io/datavis-flask-app-with-rest-api/DataVis_App/datatables/datatables.html

## Objectives
* Store project information for the Cape Town group in a sqlite database
* Perform CRUD operations on the data via requests made to a REST api
* Perform queries on data and visualise results via interactive plots
* Provide a user friendly GUI in the form of a single page web application

## Development
### Installation and setup
Create a new python environment using virtual-env or conda, *for example*:

```
conda create -n datavis-app python=3.7
conda activate datavis-app
```

Install the packages and dependencies for this repo using pip:

```
pip install requirements.txt
```

Navigate into the *datavis-app* directory:

```
cd datavis-app
```

Initialise and populate the test database:

```
python build_database.py
```

You can then start up the app with:

```
python server.py
``` 

The example database should contain enough sample data to allow you to play around with all available functionality of the app.

### Using the REST API
The REST API base url can be found at [http://localhost:5000/api](http://localhost:5000/api).

To test that it works, you can perform a request in your browser such as [http://localhost:5000/api/people](http://localhost:5000/api/people). This should return json data describing members of the team. 

The main advantage of using Swagger to build the REST API is that your API becomes self-documenting. As a result, you should be able to access all the information you require about the available API endpoints as well as their required inputs and expected outputs by visiting the [/api/ui](http://localhost:5000/api/ui) endpoint.   

### Guide to contributing
Please take note of the following general guidelines when contributing to this codebase:
* before making any changes, please create a new branch with a name describing the feature you would like to implement (e.g. 'add_metadata_filters')
* keep your commits atomic and group changes logically
* please run the 'black' code formatter on your code before you commit changes (i.e. run `black .`)
* once you are satisfied with all your changes, submit a merge request to the master branch

## Resources
This app was loosely based off of the Real Python tutorial [Building and Documenting Python REST APIs With Flask and Connexion](https://realpython.com/flask-connexion-rest-api).
